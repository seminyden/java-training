package film;

import figure.Circle;
import figure.Figure;

import figure.exceptions.*;

public class FilmCircle extends Circle implements Film {

    /** for success serializable*/
    private static final long serialVersionUID = 1L;


    /**
     * Film circle constructor, circle radius initialization
     *
     * @param radius circle radius
     * @throws IncorrectSizeException radius mustn't be less than 0 and more than 1000
     */
    public FilmCircle(double radius) throws IncorrectSizeException {
        super(radius);
    }


    /**
     * Circle constructor from a film cut from another shape
     *
     * @param filmFigure the figure from which the cut
     * @throws IncorrectSizeException side mustn't be less than 0 and more than 1000
     */
    public FilmCircle(Film filmFigure) throws IncorrectSizeException{
        super((Figure) filmFigure);
    }


    /**
     * Returns the hashcode of the circle from the film
     *
     * @return int hashcode
     */
    public int hashCode(){
        return 5 * super.hashCode();
    }
}