package film;

import figure.Figure;
import figure.Triangle;

import figure.exceptions.*;

/**
 * Class film triangle(in this case equilateral triangle)
 */
public class FilmTriangle extends Triangle implements Film {

    /** for success serializable*/
    private static final long serialVersionUID = 1L;

    /**
     * Film triangle(in this case equilateral triangle) constructor, side initialization
     *
     * @param side side
     * @throws IncorrectSizeException side mustn't be less than 0 and more than 1000
     */
    public FilmTriangle(double side) throws IncorrectSizeException {
        super(side);
    }


    /**
     * Constructor of a triangle(in this case equilateral triangle) from a film cut from another shape
     *
     * @param filmFigure the figure from which the cut
     * @throws IncorrectSizeException side mustn't be less than 0 and more than 1000
     */
    public FilmTriangle(Film filmFigure) throws IncorrectSizeException {
        super((Figure) filmFigure);
    }


    /**
     * Returns the hash code of a triangle(in this case equilateral triangle) from film
     *
     * @return int hashcode
     */
    public int hashCode(){
        return 9 * super.hashCode();
    }
}