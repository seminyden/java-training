package box.comparators;

import figure.Figure;

import java.util.Comparator;

//ShapeComparator nested in Box.class
//ColorComparator anonymous in Test.class
public class AreaComparator implements Comparator<Figure> {

    @Override
    public int compare(Figure o1, Figure o2) {

        double result = o1.getArea() - o2.getArea();

        if(result > 0) {return 1;}

        if(result < 0) {return -1;}

        return 0;
    }
}