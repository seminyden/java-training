package box;

import figure.*;
import film.*;
import paper.*;

import io.exceptions.*;
import box.exceptions.*;
import paper.paint.Color;

import static io.FileHandler.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Box<Type extends Figure>{

    /**box capacity*/
    //private int boxCapacity = 20;

    /**list with figures*/
    private ArrayList<Type> figuresList;

    /**instance box*/
    private static final Box instanceBox = new Box<>();


    private Box(){
        this.figuresList = new ArrayList<>();
    }


    /**
     * Returns an instance of the box class
     *
     * @return Box instance
     */
    public static Box getInstanceBox(){
        return instanceBox;
    }


    /**
     * Getting instance with figures from saved file
     *
     * @param pathInstFile path to file with box figures
     *
     * @return instance box with figure
     * @throws SetFileException File not found
     * @throws InstanceClassException Instance class not found
     * @throws CloseStreamException failed clos stream
     * @throws GetIOException io exception
     */
    public static Box getInstanceBoxFromFile(String pathInstFile)
            throws SetFileException, InstanceClassException,
            CloseStreamException, GetIOException{

        if(pathInstFile == null) {
            throw new NullPointerException("Path to file mustn't be null.");
        }

        instanceBox.setFiguresList(pathInstFile);

        return instanceBox;
    }

    //deserialization from file
    private void setFiguresList(String path)
            throws SetFileException, InstanceClassException,
            CloseStreamException, GetIOException{

            instanceBox.figuresList.clear();
            instanceBox.figuresList = (ArrayList<Type>) get(path);
    }


    /**
     * Write list with figures to file
     *
     * @param pathInstFile path to the file with figures
     * @throws SetFileException File not found
     * @throws CloseStreamException Failed close stream
     * @throws SaveIOException IO exception
     */
    public void saveFiguresToFile(String pathInstFile)
            throws SetFileException, CloseStreamException, SaveIOException {

        if(pathInstFile == null) {
            throw new NullPointerException("Path to file mustn't be null.");
        }

        save(figuresList,pathInstFile);
    }


    /**
     * Adding a shape to the box
     *
     * @param figure the figure to be added to the box
     * @throws FigureIsRepeatedException this figure is repeated
     */
    public <T extends Type> void addFigure(T figure) throws FigureIsRepeatedException {

        if(figure == null){
            throw new NullPointerException("Figure mustn't be null.");
        }

        for (Type inBox : figuresList) {
            if (inBox.equals(figure)) {
                throw new FigureIsRepeatedException("This figure is already in the box.");
            }
        }

        figuresList.add(figure);
    }


    /**
     * View the figures in the box by number
     *
     * @param number figure number in the box
     * @return String with the result
     * @throws BoxIsEmptyException box is empty
     */
    public Type viewFigureByNumber(int number) throws BoxIsEmptyException {

        if(figuresList.isEmpty()) {
            throw new BoxIsEmptyException("Box is empty");
        }

        if(number <= 0 || figuresList.size() < number) {
            throw new ArrayIndexOutOfBoundsException("number mustn't be less than 0 or more than figureList.size ");
        }

        return figuresList.get(--number);
    }


    /**
     * Extraction of a figure from a box by number
     *
     * @param number figure number in the box
     * @return extracted figure
     * @throws BoxIsEmptyException box is empty
     */
    public Type extractionFigure(int number) throws BoxIsEmptyException{

        if(figuresList.isEmpty()) {
            throw new BoxIsEmptyException("Box is empty.");
        }

        if(number <= 0 || figuresList.size() < number) {
            throw new ArrayIndexOutOfBoundsException("Number mustn't be less than 0 and more than figureList.size" );
        }

        Type temp = figuresList.get(--number);
        figuresList.remove(number);
        return temp;
    }

    /**
     * Figure replacement by number
     *
     * @param number figure number in the box
     * @param figure the figure to which we replace
     *
     * @return the figure that was at that place
     * @throws BoxIsEmptyException box is empty
     * @throws FigureIsRepeatedException this figure is already in the box
     */
    public <T extends Type> Type replaceFigure(int number,
                                T figure)
            throws BoxIsEmptyException, FigureIsRepeatedException {

        if(figuresList.isEmpty()) {
            throw new BoxIsEmptyException("Box is empty.");
        }

        if(figure == null) {
            throw new NullPointerException("Figure mustn't be null.");
        }

        if(number < 0 || figuresList.size() < number) {
            throw new ArrayIndexOutOfBoundsException("Position number mustn't be less than 0 and more than figureList.size()" );
        }

        for (Type inBox : figuresList) {
            if (inBox.equals(figure)) {
                throw new FigureIsRepeatedException("This figure is already in the box.");
            }
        }

        Type temp = figuresList.get(--number);
        figuresList.set(number,figure);
        return temp;
    }


    /**
     * Search by pattern
     *
     * @param figure pattern for search
     * @return Figure find result
     * @throws BoxIsEmptyException box is empty
     */
    public <T extends Type> Type toFindFigure(T figure) throws BoxIsEmptyException {

        if(figuresList.isEmpty()) {
            throw new BoxIsEmptyException("Box is empty.");
        }

        if(figure == null) {
            throw new NullPointerException("Pattern mustn't be null.");
        }

        for(Type inBox: figuresList){
            if(figure.equals(inBox)){
                return inBox;
            }
        }

        return null;
    }


    /**
     * Shows the number of shapes in a box
     *
     * @return int figures count
     */
    public int countFigures(){
        return figuresList.size();
    }


    /**
     * Calculates the total area of figures lying in a box
     *
     * @return double total area
     */
    public double totalArea(){
        double totalArea = 0;
        for (Type inBox : figuresList){
            totalArea += inBox.getArea();
        }
        return totalArea;
    }


    /**
     * Calculates the total perimeter of figures lying in a box
     *
     * @return double total perimeter
     */
    public double totalPerimetr(){
        double totalPerimetr = 0;
        for (Type inBox : figuresList){
            totalPerimetr += inBox.getPerimetr();
        }
        return totalPerimetr;
    }


    /**
     * Removing all figures in the box
     */
    public void boxClear(){
        figuresList.clear();
    }


    /**
     * Box shuffle
     */
    public void shuffle(){
        Collections.shuffle(figuresList);
    }



    //сортировка фигур по форме, площади и цвету
    public void  sort(Comparator... comparators){

        for(Comparator comparator : comparators) {
            Collections.sort(figuresList, comparator);
        }

    }


    //сложить фигуры в коробку группами
    public <T extends ArrayList<Type>>void addFiguresGroup(T figures) throws FigureIsRepeatedException {

        for (Type inBox : figuresList) {
            for(Type inAdded : figures) {
                if (inBox.equals(inAdded)) {
                    throw new FigureIsRepeatedException(inAdded.toString() + " - already in the box.");
                }
            }
        }

        figuresList.addAll(figures);
    }


    //извлчение фигур из коробки группами
    public ArrayList<Type> extractionFiguresGroup(int startIndex, int endIndex) {

        if(startIndex <= 0 ||
                figuresList.size() < startIndex ||
                endIndex <= 0 ||
                figuresList.size() < endIndex ||
                startIndex > endIndex) {

            throw new ArrayIndexOutOfBoundsException("Start and end index mustn't be less than 0 and more than figureList.size" );
        }

        ArrayList<Type> extr = new ArrayList<>();

        for(int i = --startIndex; i < endIndex; i++){
            extr.add(figuresList.get(i));
        }

        figuresList.removeAll(extr);
        return extr;
    }


    //возможность получить из коробки все заданные фигуры, все пленочные фигуры
    public ArrayList<Type> getAllFigures(Class cl){
        ArrayList<Type> figures = new ArrayList<>();

        for(Type figure : figuresList){
            if((cl).isInstance(figure)){
                figures.add(figure);
            }
        }

        return figures;
    }


    //возможность получить из коробки все заданные фигуры, все пленочные фигуры, все красные круги...
    public ArrayList<Type> getAllFigures(Class cl, Enum color){
        ArrayList<Type> figures = new ArrayList<>();

        for(Type figure : figuresList){

            boolean check = true;

            if(!(cl).isInstance(figure)){
                check = false;
            }

            if(figure instanceof Paper){
                check = ((Paper) figure).getColor().equals(color);
            }

            if(figure instanceof Film){
                check = Color.DEFAULT.equals(color);
            }

            if(check){
                figures.add(figure);
            }
        }

        return figures;
    }


    /**
     * Create description line
     * @return String description line
     */
    public String toString(){
        StringBuilder box = new StringBuilder();
        for (Type inBox : figuresList){
            box.append((figuresList.indexOf(inBox) + 1));
            box.append(" ");
            box.append(inBox.toString());
            box.append("\n");
        }
        return new String(box);
    }

    //компаратор сравнения формы фигур
    public static class ShapeComparator implements Comparator<Figure> {
        @Override
        public int compare(Figure o1, Figure o2) {
            return o1.getClass().getSuperclass().getSimpleName().compareTo(o2.getClass().getSuperclass().getSimpleName());
        }
    }
}