package paper.paint;

import java.io.Serializable;

/**
 * Paper repainting counter
 */
public class PaintCounter implements Serializable {

    /** for success serializable*/
    private static final long serialVersionUID = 1L;

    /** the number of perfect repaints of the figure*/
    private transient int paintCount;

    /** the number of allowed repainting of the figure*/
    private static int limitRepainting = 1;                                      //default 1


    /** Constructor*/
    public PaintCounter(){
        paintCount = 0;
    }


    /**
     * Returns the value of the allowed number of repaints for one shape
     *
     * @return number of allowed repainting of the shape
     */
    public int getLimitRepainting(){
        return limitRepainting;
    }


    /**
     * Increases the number of perfect repaints by 1
     */
    public void incrementPaintCount(){
        paintCount++;
    }


    /**
     * sets the number of perfect repaints of the figure
     *
     * @param count number of perfect repaints of the figure
     */
    public void setPaintCount(int count) {

        if(count < 0){
            throw new IndexOutOfBoundsException("Count mstn't be less than 0.");
        }

        this.paintCount = count;
    }

    /**
     * Returns the number of repaints of a given shape
     *
     * @return number of repaints
     */
    public int getPaintCount(){
        return paintCount;
    }
}
