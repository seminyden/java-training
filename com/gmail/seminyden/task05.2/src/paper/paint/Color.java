package paper.paint;

/**
 * Enumeration of colors in which you can paint a paper figure
 */
public enum Color {

    RED, ORANGE, YELLOW, GREEN,
    BLUE, VIOLET, DEFAULT

}
