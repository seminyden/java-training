import box.Box;
import box.comparators.*;
import figure.*;
import paper.*;
import film.*;
import paper.paint.*;

import figure.exceptions.*;

import java.util.*;

/**
 * Test class
 */
public class Test {

    private static Box box = null;


    //Создает список случаных фигур
    private static ArrayList<Figure> createRandomFigures (int count) throws IncorrectSizeException {

       ArrayList<Figure> list = new ArrayList<>();
        for(int i = 1; i <= count; i++){
            boolean randomPaint = ((int) (Math.random() * 2)) == 1;
            int randomCreate = (int) (Math.random() * 6) + 1;
            switch(randomCreate){
                case 1:
                    PaperCircle pc = new PaperCircle(i);
                    if(randomPaint){pc.setColor(randomColor().toString());}
                    list.add(pc);
                    break;
                case 2:
                    PaperRectangle pr = new PaperRectangle(i);
                    if(randomPaint){pr.setColor(randomColor().toString());}
                    list.add(pr);
                    break;
                case 3:
                    PaperTriangle pt = new PaperTriangle(i);
                    if(randomPaint){pt.setColor(randomColor().toString());}
                    list.add(pt);
                    break;
                case 4:
                    list.add(new FilmCircle(i));
                    break;
                case 5:
                    list.add(new FilmRectangle(i));
                    break;
                case 6:
                    list.add(new FilmTriangle(i));
                    break;
            }
        }

        return list;
    }


    //возвращает случайный цвет
    private static Color randomColor(){
        int random = new Random().nextInt(Color.values().length - 1);
        return Color.values()[random];
    }


    /**
     *
     * в классе PaintCounter =>     private transient int paintCount;
     *                              private static int limitRepainting = 1;
     *
     * @param args no args
     */
    public static void main(String[] args) {

        box = Box.getInstanceBox();

        System.out.println("************************ADD FIGURES TO BOX******************************");

        try {
            ArrayList<Figure> figures = createRandomFigures(50);              //количество созданных фигур
            Iterator figure = figures.iterator();

            while (figure.hasNext()) {
                box.addFigure((Figure) figure.next());
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }


        System.out.println(box);
        System.out.println();
        System.out.println("*********************SORTED ONLY BY SHAPE************************");

        Box.ShapeComparator shapeC = new Box.ShapeComparator();
        box.sort(shapeC);

        System.out.println(box);
        System.out.println();
        System.out.println("*********************SORTED ONLY BY AREA (nested comparator)************************");

        AreaComparator areaC = new AreaComparator();

        box.shuffle();
        box.sort(areaC);

        try {
            for (int i = 1; i <= 50; i++) {

                Figure temp = box.viewFigureByNumber(i);
                System.out.println(temp + " area: " + temp.getArea());
            }

        } catch (Exception ex){
            ex.printStackTrace();
        }

        System.out.println();
        System.out.println("************************SORTED ONLY BY COLOR (anonymous comparator)******************************");

        box.shuffle();

        box.sort(new Comparator<Figure>() {
            @Override
            public int compare(Figure o1, Figure o2) {

                Color first = (o1 instanceof Film) ? Color.DEFAULT : ((Paper) o1).getColor();
                Color second = (o2 instanceof Film) ? Color.DEFAULT : ((Paper) o2).getColor();

                return first.compareTo(second);
            }
        });

        System.out.println(box);
        System.out.println();
        System.out.println("********************SORTED BY AREA, SHAPE and COLOR**************************");

        box.shuffle();

        box.sort(areaC,shapeC,new Comparator<Figure>() {
            @Override
            public int compare(Figure o1, Figure o2) {

                Color first = (o1 instanceof Film) ? Color.DEFAULT : ((Paper) o1).getColor();
                Color second = (o2 instanceof Film) ? Color.DEFAULT : ((Paper) o2).getColor();

                return first.compareTo(second);
            }
        });

        try {
            for (int i = 1; i <= 50; i++) {

                Figure temp = box.viewFigureByNumber(i);
                System.out.println(temp + " area: " + temp.getArea());
            }

        } catch (Exception ex){
            ex.printStackTrace();
        }
        System.out.println();
        System.out.println("********************ADD TO BOX FIGURES GROUP**************************");

        ArrayList<Figure> added = new ArrayList<>();

        try {
            added.add(new FilmTriangle(200));
            added.add(new FilmRectangle(300));
            added.add(new PaperCircle(400));

            box.addFiguresGroup(added);

        } catch (Exception ex){
            ex.printStackTrace();
        }

        System.out.println(box);
        System.out.println();
        System.out.println("********************EXTRACTION FIGURES GROUP**************************");

        ArrayList<Figure> extr = null;

        try{

            extr = box.extractionFiguresGroup(2,7);

        } catch(Exception ex){
            System.out.println(ex);
        }

        System.out.println(box);
        System.out.println();
        System.out.println(extr);

        System.out.println();
        System.out.println("********************EXTRACT ALL FIGURES OF A GIVEN TYPES**************************");

        ArrayList<Figure> allTriangle = new ArrayList<>();
        ArrayList<Figure> allFilm = new ArrayList<>();
        ArrayList<Figure> allRedCircle = new ArrayList<>();

        try{

            allTriangle = box.getAllFigures(Triangle.class);
            allFilm = box.getAllFigures(Film.class);
            allRedCircle = box.getAllFigures(Circle.class,Color.RED);

        } catch (Exception ex){
            ex.printStackTrace();
        }

        System.out.println("*****************ALL TRIANGLE*********************");
        System.out.println(allTriangle);
        System.out.println("*****************ALL FILM*********************");
        System.out.println(allFilm);
        System.out.println("*****************ALL RED CIRCLE*********************");
        System.out.println(allRedCircle);
    }
}