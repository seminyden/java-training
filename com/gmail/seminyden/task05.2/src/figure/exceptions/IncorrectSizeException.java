package figure.exceptions;

public class IncorrectSizeException extends Exception {

    private static final long serialVersionUID = 1L;

    public IncorrectSizeException(){}

    public IncorrectSizeException(String message){
        super(message);
    }
}
