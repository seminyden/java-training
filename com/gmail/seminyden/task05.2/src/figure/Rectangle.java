package figure;

import figure.exceptions.*;

/**
 * Class rectangle(in this case, a square)
 */
public abstract class Rectangle extends Figure {

    /** for success serializable*/
    private static final long serialVersionUID = 1L;

    /**side a*/
    private double sideA;

    /**side b*/
    private double sideB;


    /**
     * Class constructor, initialization of the sides of the rectangle (in this case, a square)
     *
     * @param side sid size
     * @throws IncorrectSizeException side mustn't be less than 0 and more than 1000
     */
    public Rectangle(double side) throws IncorrectSizeException {
        setSideA(side);
        setSideB(side);
    }


    /**
     * Class constructor, for cutting a rectangle (in this case, a square) from another shape
     *
     * @param figure the figure from which they cut
     * @throws IncorrectSizeException side mustn't be less than 0 and more than 1000
     */
    public Rectangle(Figure figure) throws IncorrectSizeException {
        setSideA(calculationSize(figure));
        setSideB(sideA);
    }


    /**
     * Calculating the size of a rectangle (in this case, a square) to cut it out of another shape
     *
     * @param figure the object from which we cut
     * @return double size
     */
    public double calculationSize(Figure figure) {

        if(figure == null){
            throw new NullPointerException("Figure mustn't be null.");
        }

        return Math.sqrt(figure.getArea() / 2);                         // половина площади фигуры в которую вписываем
    }                                                                   // сторона квадрата = корень из площади


    /**
     * Check side
     *
     * @param side double side
     * @throws IncorrectSizeException side mustn't be less than 0 and more than 1000
     */
    public void checkSide(double side) throws IncorrectSizeException {

        if(side <= 0){
            throw new IncorrectSizeException("Side mustn't be less than 0: " + side + " < 0");
        }

        if(side > 1000){
            throw new IncorrectSizeException("Side mustn't be more than 1000: " + side + " > 1000");
        }
    }


    /**
     * Sets the value of side A
     *
     * @param sideA side a
     * @throws IncorrectSizeException side mustn't be less than 0 and more than 1000
     */
    public void setSideA(double sideA) throws IncorrectSizeException {

        checkSide(sideA);

        this.sideA = sideA;
    }


    /**
     * Returns the value of side A
     *
     * @return double side a
     */
    public double getSideA() {
        return sideA;
    }


    /**
     * Sets the value of side B
     *
     * @param sideB side b
     * @throws IncorrectSizeException side mustn't be less than 0 and more than 1000
     */
    public void setSideB(double sideB) throws IncorrectSizeException {

        checkSide(sideB);

        this.sideB = sideB;
    }


    /**
     * Returns the value of side B
     *
     * @return double side b
     */
    public double getSideB() {
        return sideB;
    }


    /**
     * Calculation of the area of the rectangle (in this case, a square)
     *
     * @return double rectangle area value
     */
    @Override
    public double getArea() {
        double area = sideA * sideB;
        return area;
    }


    /**
     * Calculation of the perimeter of the rectangle (in this case, a square)
     *
     * @return double perimeter value
     */
    @Override
    public double getPerimetr() {
        double perimetr = (sideA + sideB) * 2;
        return perimetr;
    }


    /**
     * Creating a line with a description of the rectangle (in this case, a square)
     *
     * @return rectangle description
     */
    @Override
    public String toString() {
        return super.toString() + ", side: a = " + getSideA() + "; b = " + getSideB();
    }


    /**
     * Check the equality of two figures
     *
     * @param object object to compare
     * @return boolean comparison result
     */
    @Override
    public boolean equals(Object object) {
        if(!super.equals(object)){
            return false;
        }
        Rectangle rectangle = (Rectangle) object;
        if(this.sideA != rectangle.sideA){
            return false;
        }
        return this.sideB == rectangle.sideB;
    }


    /**
     * Returns a hashcode of a rectangle (in this case, a square)
     *
     * @return int hashcode
     */
    @Override
    public int hashCode() {
        return (int)(15*sideA + 27*sideB);
    }
}