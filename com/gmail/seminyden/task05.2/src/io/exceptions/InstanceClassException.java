package io.exceptions;

public class InstanceClassException extends Exception {

    private static final long serialVersionUID = 1L;

    public InstanceClassException(){}

    public InstanceClassException(String message){
        super(message);
    }
}
