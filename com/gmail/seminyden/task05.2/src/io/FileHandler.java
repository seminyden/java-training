package io;

import io.exceptions.*;

import java.io.*;

/**
 * Class for serialization
 */
public abstract class FileHandler {

    /**
     * Write instance to file
     * @param object object we are writing
     * @param pathFile path to file instance
     * @throws SetFileException file not found
     * @throws SaveIOException io exception
     * @throws CloseStreamException failed close stream
     */
    public static void save(Object object,
                            String pathFile)
            throws SetFileException, SaveIOException, CloseStreamException {

        if (object == null || pathFile == null) {
            throw new NullPointerException("Object and path to file mustn't be null.");
        }

        ObjectOutputStream out = null;

        try {
            out = new ObjectOutputStream(new FileOutputStream(pathFile));
            out.writeObject(object);
            out.close();


        } catch (FileNotFoundException fnfex){
            SetFileException ex = new SetFileException("File not found.");
            ex.initCause(fnfex);
            throw ex;


        } catch (IOException ioex){
            SaveIOException ex = new SaveIOException("Failed to save instance.");
            ex.initCause(ioex);
            throw ex;


        } finally {

            if(out != null){

                try{
                    out.close();

                } catch(IOException ioex){
                    CloseStreamException ex = new CloseStreamException("Failed closed output stream.");
                    ex.initCause(ioex);
                    throw ex;
                }
            }
        }
    }


    /**
     * Get instance from a file
     *
     * @param pathInstanceFile path to instance file
     * @return Object instance
     * @throws SetFileException file not found
     * @throws GetIOException io exception
     * @throws InstanceClassException instance class not found
     * @throws CloseStreamException failed close stream
     */
    public static Object get(String pathInstanceFile)
            throws SetFileException, GetIOException, InstanceClassException, CloseStreamException {

        if (pathInstanceFile == null) {
            throw new NullPointerException("Path to instance file mustn't be null.");
        }

        ObjectInputStream in = null;
        Object instance = null;

        try {

            in = new ObjectInputStream(new FileInputStream(pathInstanceFile));
            instance = in.readObject();


        } catch (FileNotFoundException fnfex) {
            SetFileException ex = new SetFileException("File not found.");
            ex.initCause(fnfex);
            throw ex;


        } catch(ClassNotFoundException cnfex){
            InstanceClassException ex = new InstanceClassException("Class for this instance not found.");
            ex.initCause(cnfex);
            throw ex;


        } catch (IOException ioex){
            GetIOException ex = new GetIOException("Failed to get instance.");
            ex.initCause(ioex);
            throw ex;


        } finally {

            if(in != null){

                try{
                    in.close();

                } catch(IOException ioex){
                    CloseStreamException ex = new CloseStreamException("Failed closed input stream.");
                    ex.initCause(ioex);
                    throw ex;
                }
            }
        }

        return instance;
    }
}
