package element;

/**
 * Word class
 */
public class Word {

    //массив с буквами
    private char[] characters;

    //конструктор, инициализирует массив с буквами
    public Word(String word){
        word = word.trim();
        convertToChar(word);
    }

    //разлаживает слово на буквы, записывая его в массив
    private void convertToChar(String word){
        characters = word.toCharArray();
    }

    //собирает новое слово удаляя из него все вхождения первой буквы, изменяет массив с буквами
    public Word removeAllSymbolEqualsFirstSymbol(){
        StringBuilder word = new StringBuilder();
        word.append(characters[0]);

        for(int i = 1; i < characters.length; i++){
            if(characters[i] != characters[0]) {
                word.append(characters[i]);
            }
        }

        characters = null;
        convertToChar(new String(word));
        return this;
    }

    //строка
    public String toString(){
        StringBuilder word = new StringBuilder();
        for(char c : characters){
            word.append(c);
        }
        return new String(word);
    }
}