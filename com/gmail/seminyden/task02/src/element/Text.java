package element;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Text class
 */
public class Text {

    //список с предложениями
    private ArrayList<Sentence> sentencesList = new ArrayList<>();

    //шаблон который делит текст на предложения
    public static final Pattern REGEX_SENTENCE = Pattern.compile("(?<=[.!?»]\\s)");

    //конструктор, запускает метод convertToSentence
    public Text(String text){
        text.trim();
        convertToSentences(text);
    }

    //разбивает текст на предложения и добавляет в список
    public void convertToSentences(String text){
        String[] temp = text.split(REGEX_SENTENCE.toString());

        for(String sentence : temp){
            sentencesList.add(new Sentence(sentence));
        }
    }

    //для всех вопросительных предложений выполняет метод transformAllWords()
    public void forQuestionSentencesTransformWords(){
        for(Sentence sentence : sentencesList){
            if(sentence.isQuestion()){
                sentencesList.set(sentencesList.indexOf(sentence),sentence.transformAllWords());
            }
        }
    }

    //поиск слов из первого предложения, которые не повторяются в тексте, поиск чувствителен к регистру
    //возвращает список слов
    public String checkWordsInText(){
        StringBuilder words = new StringBuilder();

        for(Object element : sentencesList.get(0).getElementsList()){
            if(element instanceof Word){
                if(!checkWordInText(2,element.toString())){
                    words.append(element);
                    words.append("\n");
                }
            }
        }
        return new String(words);
    }

    //проверяет есть ли слово в тексте, начиная с заданного предложения
    private boolean checkWordInText(int numOfSentence,String word){
        for (int i = numOfSentence - 1; i < sentencesList.size(); i++){
            if(sentencesList.get(i).checkWord(word)){
                return true;
            }
        }
        return false;
    }

    //в каждом восклицательном предложении меняет местами первое и последнее слово
    public void replaceWordsInExclamatorySentence(){
        for(Sentence sentence : sentencesList){
            if(sentence.isExclamatory()){
                sentencesList.set(sentencesList.indexOf(sentence),sentence.replaceWords(0,sentence.getAmountOfElements()-1));
            }
        }
    }

    //в каждом повествовательном предложении удалить подстроку максимальной длины заданной символами
    public void removeSubstringMaxLengthInDeclarativeSentence(String begin, String end){
        for(Sentence sentence : sentencesList){
            if(sentence.isDeclarative()){
                sentencesList.set(sentencesList.indexOf(sentence),sentence.removeSubstringMaxLength(begin,end));
            }
        }
    }

    //строки с предложениями текста
    public String toString(){
        StringBuilder text = new StringBuilder();
        for(Sentence sentence : sentencesList){
            text.append(sentence);
            text.append("\n");
        }
        return new String(text);
    }
}