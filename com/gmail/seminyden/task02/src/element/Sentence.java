package element;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Sentence class
 */
public class Sentence {

    //массив с элементами предложения
    private ArrayList<Object> elementsList = new ArrayList<>();

    //шаблон разделения предложения на слова и пунктуационные знаки
    public static final Pattern REGEX_ELEMENT_DELIMITER = Pattern.compile("(?<=[а-яА-Я])(?=[\\p{Punct}\\s—])|(?<=[\\p{Punct}—])(?=\\s)|(?<=[\\p{Punct}—])(?=[\\p{Punct}—])|(?<=[\\p{Punct}—])(?=[а-яА-Я])");

    //шаблон слова
    public static final Pattern REGEX_WORD = Pattern.compile("[а-яА-Я]+");

    //шаблон вопросительного предложения
    public static final Pattern REGEX_QUESTION = Pattern.compile("\\?\\W?$");

    //шаблон восклицательного предложения
    public static final Pattern REGEX_EXCLAMATORY = Pattern.compile("!\\W?$");

    //шаблон повествовательного предложения
    public static final Pattern REGEX_DECLARATIVE = Pattern.compile("\\.\\W?$");

    //конструктор
    public Sentence(String sentence){
        sentence = sentence.trim();
        convertToElement(sentence);
    }

    //разделяет предложение на слова и знаки пунктуации
    private void convertToElement(String sentence){

        String[] temp = sentence.split(REGEX_ELEMENT_DELIMITER.toString());

        for(String element : temp){
            if(REGEX_WORD.matcher(element.toString()).find()){
                elementsList.add(new Word(element));
            } else {
                elementsList.add(new Punct(element));
            }
        }
    }

    //проверяет предложение, является ли оно вопросительным
    public boolean isQuestion(){
        if(REGEX_QUESTION.matcher(toString()).find()){
            return true;
        }
        return false;
    }

    //проверяет предложение, является ли оно восклицательным
    public boolean isExclamatory(){
        if(REGEX_EXCLAMATORY.matcher(toString()).find()){
            return true;
        }
        return false;
    }

    //проверяет предложение, является ли оно повествовательным
    public boolean isDeclarative(){
        if(REGEX_DECLARATIVE.matcher(toString()).find()){
            return true;
        }
        return false;
    }

    //возвращает список с элементами предложения
    public ArrayList<Object> getElementsList(){
        return elementsList;
    }

    //поиск слова в предложении
    public boolean checkWord(String word){

        for(Object element : elementsList){
            if(element instanceof Word){
                if (word.equalsIgnoreCase(element.toString())) {
                    return true;
                }
            }
        }
        return false;
    }

    //удаляет подстроку максимальной длинны, изменяет предложение и возращает его
    public Sentence removeSubstringMaxLength(String beginChar,
                                             String endChar){

        if(beginChar.isEmpty() || endChar.isEmpty()){
            return this;
        }

        String sentence = toString();

        int begin = sentence.indexOf(beginChar);
        int end = sentence.lastIndexOf(endChar);

        if(begin > end){
            return this;
        }

        if(begin != -1 && end != -1){
            sentence = sentence.replace(sentence.substring(begin,end + endChar.length())," ");
        }

        elementsList.clear();
        convertToElement(sentence);
        return this;
    }

    //меняет местами слова в предложении
    public Sentence replaceWords(int first,
                                 int second){

        while(elementsList.get(first) instanceof Punct){
            first++;
        }

        while(elementsList.get(second) instanceof Punct){
            second--;
        }

        Word temp = (Word) elementsList.get(first);
        elementsList.set(first,elementsList.get(second));
        elementsList.set(second,temp);

        return this;
    }

    //изменяет все слова в предложении, вызывает метод removeAllSymbolEqualsFirstSymbol для каждого слова
    public Sentence transformAllWords(){
        for(Object element : elementsList){
            if(element instanceof Word){
                elementsList.set(elementsList.indexOf(element),((Word) element).removeAllSymbolEqualsFirstSymbol());
            }
        }
        return this;
    }

    //возвращает количество элементов в предложении
    public int getAmountOfElements(){
        return elementsList.size();
    }

    //строка с предложением
    public String toString(){
        StringBuilder sentence = new StringBuilder();
        for(int i = 0; i < elementsList.size() - 1; i++){
            if(elementsList.get(i + 1) instanceof Punct) {
                sentence.append(elementsList.get(i));
            } else {
                sentence.append(elementsList.get(i));
                sentence.append(" ");
            }
        }
        sentence.append(elementsList.get(elementsList.size() - 1));
        return new String(sentence);
    }
}