package element;

public class Punct {

    //символ знака пунктуации
    String punct;

    //конструктор, инициализирует знак
    public Punct(String punct){
        this.punct = punct;
    }

    //строка со знаком
    public String toString(){
        return punct;
    }
}
