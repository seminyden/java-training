package file;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Read {

    //возвращает строку с текстом из файла
    public static String getText(String filePath) throws IOException {
        StringBuilder text = new StringBuilder();
        List<String> lines = Files.readAllLines(Paths.get(filePath));

        for(String line : lines){
            text.append(line);
            text.append(" ");
        }
        return new String(text);
    }
}