import element.Text;

import java.io.IOException;

import static file.Read.getText;

/**
 * Test class
 */
public class Test {

    private static final String FILE_PATH = ".\\src\\Text.txt";

    public static void main(String[] args) throws IOException {

        //создает объект класса Text передает в конструктор строку с текстом
        //из файла FILE_PATH, через метод класса file.Read
        Text task = new Text(getText(FILE_PATH));

        System.out.println("Текст:");
        System.out.println();
        System.out.println(task);

        System.out.println("**************************************************************************");
        System.out.println("1.Во всех вопросительных предложениях текста преобразовать каждое слово, " +
                "удалив из него все последующие вхождения первой буквы этого слова:");
        task.forQuestionSentencesTransformWords();
        System.out.println();
        System.out.println(task);

        System.out.println("**************************************************************************");
        System.out.println("2. Слова первого предложения, которых нет в других предложениях: ");
        System.out.println();
        System.out.println(task.checkWordsInText());

        System.out.println("**************************************************************************");
        System.out.println("3. В каждом восклицательном предложении текста поменять местами первое слово с последним:");
        task.replaceWordsInExclamatorySentence();
        System.out.println();
        System.out.println(task);

        System.out.println("**************************************************************************");
        System.out.println("4. В каждом повествовательном предложении текста исключить подстроку " +
                "максимальной длины, начинающуюся и заканчивающуюся заданными символами:");
        task.removeSubstringMaxLengthInDeclarativeSentence("о","а");
        System.out.println();
        System.out.println(task);
    }
}