package paper.paint;

/**
 * Enumeration of colors in which you can paint a paper figure
 */
public enum Color {

    Red, Orange, Yellow, Green,
    Blue, Violet, Default

}
