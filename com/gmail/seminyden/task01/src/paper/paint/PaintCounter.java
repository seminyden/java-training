package paper.paint;

/**
 * Paper repainting counter
 */
public class PaintCounter {

    /**
     * the number of perfect repaints of the figure
     */
    private int paintCount;

    /**
     * the number of allowed repainting of the figure
     */
    private int canPainted;


    /**
     * Constructor, initialization of values
     */
    public PaintCounter(){
        this.paintCount = 0;
        this.canPainted = 1;                                                        //default 1
    }


    /**
     * Sets the value of the allowed number of repaints of one shape
     *
     * @param count number of allowed repainting of the shape
     */
    public void setCanPainted(int count){
        canPainted = count;
    }


    /**
     * Returns the value of the allowed number of repaints for one shape
     *
     * @return number of allowed repainting of the shape
     */
    public int getCanPainted(){
        return canPainted;
    }


    /**
     * Increases the number of perfect repaints by 1
     */
    public void incrementPaintCount(){
        paintCount++;
    }


    /**
     * sets the number of perfect repaints of the figure
     *
     * @param count number of perfect repaints of the figure
     */
    public void setPaintCount(int count){
        this.paintCount = count;
    }

    /**
     * Returns the number of repaints of a given shape
     *
     * @return number of repaints
     */
    public int getPaintCount(){
        return paintCount;
    }
}
