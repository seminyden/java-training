package paper;

import paper.paint.Color;

public interface Paper {

    /**
     * Repaints the shape to the specified color
     *
     * @param color color to repaint the shape
     */
    void setColor(String color);

    /**
     * Returns the color of the shape
     *
     * @return Color shape color
     */
    Color getColor();

    /**
     * Returns the number of repaints of a given shape
     *
     * @return number of repaints
     */
    int getPaintCount();
}