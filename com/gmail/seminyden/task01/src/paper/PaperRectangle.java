package paper;

import figure.Figure;
import figure.Rectangle;
import paper.paint.*;


/**
 * Class paper rectangle(in this case square)
 */
public class PaperRectangle extends Rectangle implements Paper {

    /**
     * repainting counter creation
     */
    private PaintCounter paintCounter = new PaintCounter();

    /**
     * sets the default shape color
     */
    private Color color = Color.Default;


    /**
     * Paper rectangle(in this case square) constructor, side initialization
     *
     * @param side side
     */
    public PaperRectangle(double side) {
        super(side);
    }


    /**
     * Designer of a rectangle(in this case square) of paper cut from another shape
     *
     * @param paperFigure the figure from which the cut
     */
    public PaperRectangle(Paper paperFigure){
        super((Figure) paperFigure);
        this.color = paperFigure.getColor();
        this.paintCounter.setPaintCount(paperFigure.getPaintCount());
    }


    /**
     * Returns the number of repaints of a given shape
     *
     * @return number of repaints
     */
    public int getPaintCount(){
        return paintCounter.getPaintCount();
    }


    /**
     * Repaints the rectangle(in this case square) to the specified color
     *
     * @param color color to repaint the shape
     */
    @Override
    public void setColor(String color) {
        if(paintCounter.getPaintCount() < paintCounter.getCanPainted()) {
            this.color = Color.valueOf(color);
            paintCounter.incrementPaintCount();
        }
    }


    /**
     * Returns the color value of the rectangle(in this case square)
     *
     * @return rectangle(in this case square) color
     */
    @Override
    public Color getColor() {
        return color;
    }


    /**
     * Create a paper rectangle(in this case square) description line
     *
     * @return String description string
     */
    public String toString(){
        return super.toString() + " color: " + this.getColor();
    }


    /**
     * Returns a hashcode of a paper rectangle(in this case square)
     *
     * @return int hashcode
     */
    public int hashCode(){
        return 13 * super.hashCode();
    }
}