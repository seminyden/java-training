package figure;


/**
 * General class for figures
 */
public abstract class Figure {

    /**
     * Calculation of the area of the figure
     *
     * @return double area of the figure
     */
    public abstract double getArea();

    /**
     * Calculation of the perimetr of the figure
     *
     * @return double perimetr of the figure
     */
    public abstract double getPerimetr();

    /**
     * Calculating the size of the cut shape from another shape
     *
     * @param figure the object from which we cut
     * @return size figure for the cutting
     */
    public abstract double calculationSize(Figure figure);

    /**
     * Creating a line for the description of the figure
     *
     * @return String describing the shape
     */
    public String toString(){
        return this.getClass().getSimpleName();
    }

    /**
     * Checking the equality of two objects
     *
     * @param object object to compare
     * @return boolean equality test result
     */
    public boolean equals(Object object){
        if(this == object){
            return true;
        }
        if(object == null){
            return false;
        }
        return this.getClass() == object.getClass();
    }
}