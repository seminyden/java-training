package figure;


/**
 * Class circle
 */
public abstract class Circle extends Figure{

    /**
     * radius of the circle
     */
    private double radius;


    /**
     * Class constructor, initializes the radius of the circle
     *
     * @param radius initialization of circle radius
     */
    public Circle(double radius) {
        this.radius = radius;
    }


    /**
     * Class constructor, for cutting a circle from another shape
     *
     * @param figure the figure from which they cut
     */
    public Circle(Figure figure){
        this.radius = calculationSize(figure);
    }


    /**
     * Calculating the radius of a circle to cut it out of another shape
     *
     */
    public double calculationSize(Figure figure){
        return Math.sqrt((figure.getArea() / 2) / Math.PI);         //площадь фигуры в которую вписываем делим на два
    }                                                               //по формуле находим радиус круга через площадь


    /**
     * Sets the radius value
     *
     * @param radius circle radius
     */
    public void setRadius(double radius){
        this.radius = radius;
    }


    /**
     * Returns the radius of a circle
     *
     * @return double radius of a circle
     */
    public double getRadius() {
        return radius;
    }


    /**
     * Calculation of the area of a circle
     *
     * @return double value of the area of the circle
     */
    @Override
    public double getArea() {
        double area = Math.PI * radius *radius;
        return area;
    }


    /**
     * Calculation of circle circumference
     *
     * @return double value of circle circumference
     */
    @Override
    public double getPerimetr() {
        double perimetr = 2 * Math.PI * radius;
        return perimetr;
    }


    /**
     * Creating a circle description line
     *
     * @return String line of circle description
     */
    public String toString(){
        return super.toString() + ", radius = " + getRadius();
    }


    /**
     * Checking the equality of two objects
     *
     * @param object object to compare
     * @return result of comparing two figures
     */
    @Override
    public boolean equals(Object object){
        if(!super.equals(object)){
            return false;
        }
        Circle circle = (Circle) object;
        return this.getRadius() == circle.getRadius();
    }

    /**
     * Returns a hashcode of a shape
     *
     * @return int hashcode
     */
    @Override
    public int hashCode() {
        return (int)(17*radius);
    }
}