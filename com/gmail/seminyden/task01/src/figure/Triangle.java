package figure;

/**
 *
 * Class triangle(in this case equilateral triangle)
 *
 */
public abstract class Triangle extends Figure{

    /**
     * side a
     */
    private double sideA;

    /**
     * side b
     */
    private double sideB;

    /**
     * side c
     */
    private double sideC;


    /**
     *
     * Class constructor, initialization of the sides of the triangle(in this case equilateral triangle)
     *
     * @param side side
     */
    public Triangle(double side){
        this.sideA = side;
        this.sideB = side;
        this.sideC = side;
    }


    /**
     * Class constructor, cutting a triangle(in this case equilateral triangle) from another shape
     * @param figure the figure from which we cut the triangle(in this case equilateral triangle)
     */
    public Triangle (Figure figure){
        this.sideA = calculationSize(figure);
        this.sideB = this.sideA;
        this.sideC = this.sideA;
    }


    /**
     * Calculation of the size of a triangle (in this case, an equilateral triangle) to cut it out of another shape     *
     */
    public double calculationSize(Figure figure){
        return Math.sqrt(((figure.getArea() / 4) * 4) / Math.sqrt(3));   //площадь фигуры в которую вписываем делим на четыре
    }                                                                    //сторону находим по формуле для равностороннего треугольника
                                                                         //через площадь

    /**
     * Sets the value of side A
     *
     * @param sideA side a
     */
    public void setSideA(double sideA){
        this.sideA = sideA;
    }


    /**
     * Returns the value of side A
     *
     * @return double side a
     */
    public double getSideA() {
        return sideA;
    }


    /**
     * Sets the value of side B
     *
     * @param sideB side b
      */
    public void setSideB(double sideB) {
        this.sideB = sideB;
    }


    /**
     * Returns the value of side B
     *
     * @return double side b
     */
    public double getSideB() {
        return sideB;
    }


    /**
     * Sets the value of side C
     *
     * @param sideC side c
     */
    public void setSideC(double sideC) {
        this.sideC = sideC;
    }


    /**
     * Returns the value of side C
     *
     * @return double side c
     */
    public double getSideC() {
        return sideC;
    }


    /**
     * Calculation of the area of a triangle(in this case equilateral triangle)
     *
     * @return double the value of the area of the triangle(in this case equilateral triangle)
     */
    @Override
    public double getArea() {
        double p = (sideA + sideB + sideC)/2;
        double area = Math.sqrt(p * (p - sideA) * (p - sideB) * (p - sideC));
        return area;
    }


    /**
     * Calculation of the perimetr of a triangle(in this case equilateral triangle)
     *
     * @return double the value of the perimetr of the triangle(in this case equilateral triangle)
     */
    @Override
    public double getPerimetr() {
        double perimetr = sideA + sideB + sideC;
        return perimetr;
    }


    /**
     * Creating a line describing a triangle(in this case equilateral triangle)
     *
     * @return String description string of a triangle(in this case equilateral triangle)
     */
    @Override
    public String toString() {
        return super.toString() + ", side: a = " + getSideA() + ", b = " + getSideB() + ", c = " + getSideC();
    }


    /**
     * Check the equality of two figures
     *
     * @param object object to compare
     * @return boolean verification result
     */
    @Override
    public boolean equals(Object object) {
        if(!super.equals(object)){
            return false;
        }
        Triangle triangle = (Triangle) object;
        if(this.sideA != triangle.sideA){
            return false;
        }
        if(this.sideB != triangle.sideB){
            return false;
        }
        return this.sideC == triangle.sideC;
    }


    /**
     * Returns the hash code of a triangle(in this case equilateral triangle)
     *
     * @return int hashcode
     */
    public int hashCode() {
        return (int)(19*sideA + 23*sideB + 31*sideC);
    }
}