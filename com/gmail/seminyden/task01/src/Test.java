import box.*;
import figure.*;
import paper.*;
import film.*;

import java.util.ArrayList;

/**
 * Test class
 */
public class Test {

    //создает 22 случайные фигуры
    private static ArrayList createFigures(){
        ArrayList<Figure> listWithFigures = new ArrayList<>();
        for (int i = 1; i <= 22; i++) {
            int u = (int) (Math.random() * 6);
            switch (u){
                case 1:
                    listWithFigures.add(new PaperCircle(i));
                    break;
                case 2:
                    listWithFigures.add(new PaperRectangle(i));
                    break;
                case 3:
                    listWithFigures.add(new PaperTriangle(i));
                    break;
                case 4:
                    listWithFigures.add(new FilmCircle(i));
                    break;
                case 5:
                    listWithFigures.add(new FilmRectangle(i));
                    break;
                case 0:
                    listWithFigures.add(new FilmTriangle(i));
                    break;
            }
        }
        for (Figure inList : listWithFigures){
            System.out.println((listWithFigures.indexOf(inList) + 1) + " " + inList.toString());
        }
        return listWithFigures;
    }


    /**
     * compliance verification
     *
     * @param args no args
     */
    public static void main(String[] args){

        //создаем 22 случайных фигуры и выводим их в консоль
        ArrayList<Figure> listWithFigures = createFigures();

        //получаем экзмепляр коробки
        Box box = Box.getInstanceBox();

        System.out.println("\n 1 box. ***********************************************************************************");

        //каждую созданную фигуру из 22 добавляем в коробку в которой 20 мест
        //проверяем возможность добавления одной и той же фигуры в коробку
        System.out.println(box.addFigure(listWithFigures.get(0)));
        System.out.println(box.addFigure(listWithFigures.get(0)));
        for (Figure inList : listWithFigures){
            System.out.print((listWithFigures.indexOf(inList) + 1) + " " + box.addFigure(inList) + "\n");
        }

        System.out.println("\n2 box. ***********************************************************************************");

        //просмотр фигуры по номеру, фигура остается в коробке
        System.out.println(box.viewFigureByNumber(10));

        System.out.println("\n3 box. ***********************************************************************************");

        //извлекаем фигуру под номером 5, проверяем удаление из коробки и добавление в переменную extractedFigure
        Figure extractedFigure = box.extractionFigure(5);
        System.out.println("Extracted figure: " + extractedFigure);
        System.out.println(box);

        System.out.println("\n4 box. ***********************************************************************************");

        //создаем фигуру, меняем местами созданную фигуру и фигуру из коробки по номеру
        //проверяем фигуру до и после замены
        Figure replaceFigure = new FilmRectangle(99);
        System.out.println("before: " + replaceFigure);
        System.out.println(box.viewFigureByNumber(10));
        replaceFigure = box.replaceFigure(10, replaceFigure);
        System.out.println("after: " + replaceFigure);
        System.out.println(box.viewFigureByNumber(10));

        System.out.println("\n5 box. ***********************************************************************************");

        //ищем фигуру по образцу, эквивалентную своим характеристикам
        System.out.println(listWithFigures.get(9));
        System.out.println(box.toFindFigure(listWithFigures.get(9)));

        System.out.println("\n6 box. ***********************************************************************************");

        //показываем наличное кол-во фигур
        System.out.println(box.countFigure());

        System.out.println("\n7 box. ***********************************************************************************");

        //показываем суммарную площадь
        System.out.println(box.totalArea());

        System.out.println("\n8 box. ***********************************************************************************");

        //показываем суммарный периметр
        System.out.println(box.totalPerimetr());

        System.out.println("\n9 box. ***********************************************************************************");

        //извлекаем все круги из коробки, при этом круги остаются в коробке, а их копии помещаются в новый ArrayList
        //проверяем добавление кругов в новый список
        ArrayList<Figure> circleList = box.extractionAllCircles();
        for (Figure inList : circleList) {
            System.out.println((circleList.indexOf(inList)+1) + " " + inList.toString());
        }

        System.out.println("\n10 box. ***********************************************************************************");

        //извлекаем все пленочные фигуры из коробки, при этом они остаются в коробке, а их копии добаляются в новый список
        //проверяем добавление пленочных фигур в новый список
        ArrayList<Figure> filmList = box.extractionAllFilmFigures();
        for (Figure inList : filmList) {
            System.out.println((filmList.indexOf(inList)+1) + " " + inList.toString());
        }

        System.out.println("\n11 cutting. ***********************************************************************************");

        //проверяем возможность создания фигур и вырезания из них других фигур
        //проверяем перекрашивание (разрешено красить только один раз) фигур из бумаги
        //и сохранение цвета при вырезании фигур из других фигур из бумаги

        //создаем пленочные фигуры и вырезаем из них другие пленочные фигуры
        Film cutf1 = new FilmRectangle(10);
        Film cutf2 = new FilmTriangle(12);
        Film cutf3 = new FilmCircle(19);

        cutf1 = new FilmCircle(cutf1);
        System.out.println("cutting 1: " + cutf1);

        cutf2 = new FilmTriangle(cutf2);
        System.out.println("cutting 2: " + cutf2);

        cutf3 = new FilmRectangle(cutf3);
        System.out.println("cutting 3: " + cutf3);

        //создаем фигуры из бумаги и вырезаем из них другие фигуры
        //проверяем перекрашивание фигур из бумаги и сохранение цвета при вырезании из них других фигур из бумаги
        Paper cutp1 = new PaperCircle(10);
        cutp1.setColor("Green");
        cutp1.setColor("Blue");
        cutp1.setColor("Default");
        Paper cutp2 = new PaperRectangle(14);
        cutp2.setColor("Yellow");
        cutp2.setColor("Red");
        Paper cutp3 = new PaperTriangle(22);
        cutp3.setColor("Violet");
        cutp3.setColor("Orange");
        cutp3.setColor("Red");
        cutp3.setColor("Orange");


        cutp1 = new PaperCircle(cutp1);
        System.out.println("cutting 1: " + cutp1);

        cutp2 = new PaperTriangle(cutp2);
        System.out.println("cutting 2: " + cutp2);
        cutp2.setColor("Green");
        cutp2.setColor("Red");
        cutp2.setColor("Blue");
        System.out.println(cutp2);

        cutp3 = new PaperRectangle(cutp3);
        System.out.println("cutting 3: " + cutp3);
        cutp3.setColor("Red");
        cutp3.setColor("Blue");
        System.out.println(cutp3);
    }
}
