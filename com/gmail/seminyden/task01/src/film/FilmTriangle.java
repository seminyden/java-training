package film;

import figure.Figure;
import figure.Triangle;

/**
 * Class film triangle(in this case equilateral triangle)
 */
public class FilmTriangle extends Triangle implements Film {


    /**
     * Film triangle(in this case equilateral triangle) constructor, side initialization
     *
     * @param side side
     */
    public FilmTriangle(double side) {
        super(side);
    }


    /**
     * Constructor of a triangle(in this case equilateral triangle) from a film cut from another shape
     *
     * @param filmFigure the figure from which the cut
     */
    public FilmTriangle(Film filmFigure){
        super((Figure) filmFigure);
    }


    /**
     * Returns the hash code of a triangle(in this case equilateral triangle) from film
     *
     * @return int hashcode
     */
    public int hashCode(){
        return 9 * super.hashCode();
    }
}