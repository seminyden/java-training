package io;

import box.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Class for serialization
 */
public abstract class FileHandler {

    /**
     * Write instance to file
     *
     * @param pathFile path to file with instance
     * @throws Exception IOException
     */
    public static void save(Object object,String pathFile) throws Exception{

        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(pathFile));
        out.writeObject(object);
        out.close();

    }

    /**
     * Get instance from a file
     *
     * @param pathInstanceFile path to instance file
     * @return Object instance
     * @throws Exception IOException
     */
    public static Object get(String pathInstanceFile) throws Exception{

        Object instance = null;

        ObjectInputStream in = new ObjectInputStream(new FileInputStream(pathInstanceFile));
        instance = in.readObject();

        return instance;
    }
}
