package box;

import figure.Circle;
import figure.Figure;
import film.Film;

import static io.FileHandler.*;

import java.util.ArrayList;

public class Box{

    /**box capacity*/
    private int boxCapacity;

    /**list with figures*/
    private ArrayList<Figure> figuresList;

    /**instance box*/
    private static final Box instanceBox = new Box(20);


    /**
     * Class constructor, initialization of box capacity
     *
     * @param boxCapacity number of figures that can be put in a box
     */
    private Box(int boxCapacity){
        this.boxCapacity = boxCapacity;
        this.figuresList = new ArrayList<>();
    }


    /**
     * Returns an instance of the box class
     *
     * @return Box instance
     */
    public static Box getInstanceBox(){
        return instanceBox;
    }


    /**
     * Getting instance with figures from saved file
     *
     * @param pathInstFile path to file with box figures
     * @return instance box with figure
     * @throws Exception IOException
     */
    public static Box getInstanceBoxFromFile(String pathInstFile) throws Exception{
        instanceBox.figuresList.clear();
        instanceBox.figuresList = (ArrayList<Figure>) get(pathInstFile);
        return instanceBox;
    }


    /**
     * Write list with figures to file
     *
     * @param pathInstFile path to the file with figures
     * @throws Exception IOEception
     */
    public void saveFiguresToFile(String pathInstFile) throws Exception{
        save(figuresList,pathInstFile);
    }


    /**
     * Adding a shape to the box
     *
     * @param figure the figure to be added to the box
     * @return String with result of adding
     */
    public String addFigure(Figure figure){
        if(figuresList.size() >= boxCapacity){
            return "Box is full.";
        }
        for (Figure inBox : figuresList) {
            if (inBox.equals(figure)) {
                return "This figure is already in the box.";
            }
        }
        figuresList.add(figure);
        return "Figure: " + figure.toString() + " added.";
    }


    /**
     * View the figures in the box by number
     *
     * @param number figure number in the box
     * @return String with the result
     */
    public String viewFigureByNumber(int number){
        if(figuresList.isEmpty()) {
            return "Box is empty.";
        }
        if(number > 0 && number <= figuresList.size()) {
            return "Figure number " + number + ": " + figuresList.get(--number).toString();
        }
        return "Invalid number";
    }


    /**
     * Extraction of a figure from a box by number
     *
     * @param number figure number in the box
     * @return extracted figure
     */
    public Figure extractionFigure(int number){
        if(figuresList.isEmpty()) {
            return null;
        }
        if(number > 0 && number <= figuresList.size()) {
            Figure temp = figuresList.get(--number);
            figuresList.remove(number);
            return temp;
        }
        return null;
    }


    /**
     * Figure replacement by number
     *
     * @param number figure number in the box
     * @param figure the figure to which we replace
     * @return the figure that was at that place
     */
    public Figure replaceFigure(int number,
                                Figure figure){
        if(figuresList.isEmpty()) {
            return null;
        }
        for (Figure inBox : figuresList) {
            if (inBox.equals(figure)) {
                return null;
            }
        }
        if(number > 0 && number <= figuresList.size()) {
            Figure temp = figuresList.get(--number);
            figuresList.set(number,figure);
            return temp;
        } else
            return null;
    }


    /**
     * Search by pattern
     *
     * @param figure pattern for search
     * @return String search result string
     */
    public String toFindFigure(Figure figure){
        for(Figure inBox: figuresList){
            if(figure.equals(inBox)){
                return "Figure found: " + inBox;
            }
        }
        return "Not found";
    }


    /**
     * Shows the number of shapes in a box
     *
     * @return String string with the result
     */
    public String countFigure(){
        if(figuresList.isEmpty()){
            return "Box is empty.";
        }
        return "Total count figures: " + figuresList.size();
    }


    /**
     * Calculates the total area of figures lying in a box
     *
     * @return String string with total area
     */
    public String totalArea(){
        double totalArea = 0;
        for (Figure inBox : figuresList){
            totalArea += inBox.getArea();
        }
        return "Total area = " + totalArea;
    }


    /**
     * Calculates the total perimeter of figures lying in a box
     *
     * @return String string with total perimeter
     */
    public String totalPerimetr(){
        double totalPerimetr = 0;
        for (Figure inBox : figuresList){
            totalPerimetr += inBox.getPerimetr();
        }
        return "Total perimetr = " + totalPerimetr;
    }


    /**
     * Extraction all circles from the box
     *
     * @return ArrayList with all circles from the box
     */
    public ArrayList extractionAllCircles(){
        ArrayList<Circle> circleList = new ArrayList<>();
        if(figuresList.isEmpty()){
            return null;
        }
        for(Figure inBox : figuresList){
            if(inBox instanceof Circle){
                circleList.add((Circle) inBox);
            }
        }
        return circleList;
    }


    /**
     * Extraction all film shapes from the box
     *
     * @return ArrayList with all film shapes from the box
     */
    public ArrayList extractionAllFilmFigures(){
        ArrayList<Film> filmList = new ArrayList<>();
        if(figuresList.isEmpty()){
            return null;
        }
        for(Figure inBox : figuresList){
            if(inBox instanceof Film){
                filmList.add((Film) inBox);
            }
        }
        return filmList;
    }

    //removing all figures in th box
    public void boxClear(){
        figuresList.clear();
    }

    /**
     * Create description line
     * @return String description line
     */
    public String toString(){
        StringBuilder box = new StringBuilder();
        for (Figure inBox : figuresList){
            box.append((figuresList.indexOf(inBox) + 1));
            box.append(" ");
            box.append(inBox.toString());
            box.append("\n");
        }
        return new String(box);
    }
}