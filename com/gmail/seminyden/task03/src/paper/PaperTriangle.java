package paper;

import figure.Figure;
import figure.Triangle;
import paper.paint.Color;
import paper.paint.PaintCounter;


/**
 * Class paper triangle(in this case equilateral triangle)
 */
public class PaperTriangle extends Triangle implements Paper {

    /** for success serializable*/
    private static final long serialVersionUID = 1L;

    /** repainting counter creation*/
    private PaintCounter paintCounter = new PaintCounter();

    /** sets the default shape color*/
    private Color color;


    /**
     * Paper triangle(in this case equilateral triangle) constructor, side initialization
     *
     * @param side side
     */
    public PaperTriangle(double side) {
        super(side);
    }


    /**
     * Constructor of a triangle(in this case equilateral triangle) made of paper cut from another shape
     *
     * @param paperFigure the figure from which the cut
     */
    public PaperTriangle(Paper paperFigure){
        super((Figure) paperFigure);
        this.color = paperFigure.getColor();
        this.paintCounter.setPaintCount(paperFigure.getPaintCount());
    }


    /**
     * Returns the number of repaints of a given shape
     *
     * @return number of repaints
     */
    public int getPaintCount(){
        return paintCounter.getPaintCount();
    }


    /**
     * Repaint the triangle(in this case equilateral triangle) in the given color
     *
     * @param color color to repaint the shape
     */
    @Override
    public void setColor(String color) {
        if(paintCounter.getPaintCount() < paintCounter.getLimitRepainting()) {
            this.color = Color.valueOf(color);
            paintCounter.incrementPaintCount();
        }
    }


    /**
     * Returns the color value of a triangle(in this case equilateral triangle)
     *
     * @return Color color value of the triangle(in this case equilateral triangle)
     */
    @Override
    public Color getColor() {
        return color;
    }


    /**
     * Create a paper triangle(in this case equilateral triangle) description line
     *
     * @return String description string
     */
    public String toString(){
        return super.toString() + ", color: " + this.getColor() + ", repainted: " + this.paintCounter.getPaintCount() +
                ", limitRepainting: " + this.paintCounter.getLimitRepainting();
    }


    /**
     * Returns a hashcode of a triangle(in this case equilateral triangle) from paper
     *
     * @return int hashcode
     */
    public int hashCode(){
        return 11 * super.hashCode();
    }
}