package film;

import figure.Figure;
import figure.Rectangle;


/**
 * Class film rectangle(in this case square)
 */
public class FilmRectangle extends Rectangle implements Film {

    /** for success serializable*/
    private static final long serialVersionUID = 1L;

    /**
     * Film rectangle(in this case square) constructor, initialization of sides
     *
     * @param side side
     */
    public FilmRectangle(double side) {
        super(side);
    }


    /**
     * Constructor of a rectangle(in this case square) from a film cut from another shape
     *
     * @param filmFigure the figure from which the cut
     */
    public FilmRectangle(Film filmFigure){
        super((Figure) filmFigure);
    }


    /**
     * Returns the hash of the rectangle(in this case square) from the film
     *
     * @return int hashcode
     */
    public int hashCode(){
        return 7 * super.hashCode();
    }
}