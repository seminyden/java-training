package film;

import figure.Circle;
import figure.Figure;

public class FilmCircle extends Circle implements Film {

    /** for success serializable*/
    private static final long serialVersionUID = 1L;

    /**
     * Film circle constructor, circle radius initialization
     *
     * @param radius circle radius
     */
    public FilmCircle(double radius) {
        super(radius);
    }


    /**
     * Circle constructor from a film cut from another shape
     *
     * @param filmFigure the figure from which the cut
     */
    public FilmCircle(Film filmFigure){
        super((Figure) filmFigure);
    }


    /**
     * Returns the hashcode of the circle from the film
     *
     * @return int hashcode
     */
    public int hashCode(){
        return 5 * super.hashCode();
    }
}