import box.Box;
import figure.*;
import io.FileHandler;
import paper.*;
import film.*;
import paper.paint.Color;
import io.FileHandler.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * Test class
 */
public class Test {

    static Box box = null;

    //Создает список случаных фигур
    private static ArrayList<Figure> createRandomFigures (int count){

       ArrayList<Figure> list = new ArrayList<>();
        for(int i = 1; i <= count; i++){
            int random = (int) (Math.random() * 6) + 1;

            switch(random){
                case 1:
                    PaperCircle pc = new PaperCircle(i);
                    pc.setColor(randomColor().toString());
                    list.add(pc);
                    break;
                case 2:
                    PaperRectangle pr = new PaperRectangle(i);
                    pr.setColor(randomColor().toString());
                    list.add(pr);
                    break;
                case 3:
                    PaperTriangle pt = new PaperTriangle(i);
                    pt.setColor(randomColor().toString());
                    list.add(pt);
                    break;
                case 4:
                    list.add(new FilmCircle(i));
                    break;
                case 5:
                    list.add(new FilmRectangle(i));
                    break;
                case 6:
                    list.add(new FilmTriangle(i));
                    break;
            }
        }

        return list;
    }


    //возвращает случайный цвет
    public static Color randomColor(){
        int random = new Random().nextInt(Color.values().length);
        return Color.values()[random];
    }


    /**
     *
     * в классе PaintCounter =>     private transient int paintCount;
     *                              private static int limitRepainting = 1;
     *
     * @param args no args
     */
    public static void main(String[] args) throws Exception{

        box = Box.getInstanceBox();
        System.out.println("hashcode " + box.hashCode());

        System.out.println("****************** ADDED FIGURES #1 *********************");

        ArrayList<Figure> figures = createRandomFigures(18);
        Iterator figure = figures.iterator();

        while(figure.hasNext()){
            box.addFigure((Figure)figure.next());
        }

        figure = null;

        System.out.println("hashcode " + box.hashCode());
        System.out.println(box);

        System.out.println("****************** SERIALIZATION 'src\\instance\\boxFigures' ********************");

        box.saveFiguresToFile("src\\instance\\boxFigures");

        System.out.println("************** REMOVED ALL FIGURES *******************");

        box.boxClear();

        System.out.println("hashcode " + box.hashCode());
        System.out.println(box);

        System.out.println("****************** ADDED FIGURES #2 *********************");

        figures = createRandomFigures(18);
        figure = figures.iterator();

        while(figure.hasNext()){
            box.addFigure((Figure)figure.next());
        }

        figure = null;

        System.out.println("hashcode " + box.hashCode());
        System.out.println(box);

        System.out.println("****************** SERIALIZATION 'src\\instance\\boxFigures2' ********************");

        box.saveFiguresToFile("src\\instance\\boxFigures2");

        System.out.println("************** REMOVED ALL FIGURES *******************");

        box.boxClear();

        System.out.println("hashcode " + box.hashCode());
        System.out.println(box);

        System.out.println("**************** GET INSTANCE BOX #1 'src\\instance\\boxFigures' ********************");

        box = Box.getInstanceBoxFromFile("src\\instance\\boxFigures");

        System.out.println("hashcode " + box.hashCode());
        System.out.println(box);

        box = null;

        System.out.println("**************** GET INSTANCE BOX #2 'src\\instance\\boxFigures2' ********************");

        box = Box.getInstanceBoxFromFile("src\\instance\\boxFigures2");

        System.out.println("hashcode " + box.hashCode());
        System.out.println(box);

        box = null;

        System.out.println("**************** CHECK HASHCODE after box=null ********************");

        box = Box.getInstanceBox();

        System.out.println("hashcode " + box.hashCode());
        System.out.println(box);
    }
}