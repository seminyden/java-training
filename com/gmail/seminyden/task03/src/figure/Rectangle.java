package figure;


/**
 * Class rectangle(in this case, a square)
 */
public abstract class Rectangle extends Figure {

    /** for success serializable*/
    private static final long serialVersionUID = 1L;

    /**side a*/
    private double sideA;

    /**side b*/
    private double sideB;


    /**
     * Class constructor, initialization of the sides of the rectangle (in this case, a square)
     *
     * @param side side
     */
    public Rectangle(double side){
        this.sideA = side;
        this.sideB = side;
    }


    /**
     * Class constructor, for cutting a rectangle (in this case, a square) from another shape
     *
     * @param figure the figure from which they cut
     */
    public Rectangle(Figure figure){
        this.sideA = calculationSize(figure);
        this.sideB = this.sideA;
    }


    /**
     * Calculating the size of a rectangle (in this case, a square) to cut it out of another shape
     *
     */
    public double calculationSize(Figure figure){
        return Math.sqrt(figure.getArea() / 2);                         // половина площади фигуры в которую вписываем
    }                                                                   // сторона квадрата = корень из площади


    /**
     * Sets the value of side A
     *
     * @param sideA side a
     */
    public void setSideA(double sideA) {
        this.sideA = sideA;
    }


    /**
     * Returns the value of side A
     *
     * @return double side a
     */
    public double getSideA() {
        return sideA;
    }


    /**
     * Sets the value of side B
     *
     * @param sideB side b
     */
    public void setSideB(double sideB) {
        this.sideB = sideB;
    }


    /**
     * Returns the value of side B
     *
     * @return double side b
     */
    public double getSideB() {
        return sideB;
    }


    /**
     * Calculation of the area of the rectangle (in this case, a square)
     *
     * @return double rectangle area value
     */
    @Override
    public double getArea() {
        double area = sideA * sideB;
        return area;
    }


    /**
     * Calculation of the perimeter of the rectangle (in this case, a square)
     *
     * @return double perimeter value
     */
    @Override
    public double getPerimetr() {
        double perimetr = (sideA + sideB) * 2;
        return perimetr;
    }


    /**
     * Creating a line with a description of the rectangle (in this case, a square)
     *
     * @return rectangle description
     */
    @Override
    public String toString() {
        return super.toString() + ", side: a = " + getSideA() + "; b = " + getSideB();
    }


    /**
     * Check the equality of two figures
     *
     * @param object object to compare
     * @return boolean comparison result
     */
    @Override
    public boolean equals(Object object) {
        if(!super.equals(object)){
            return false;
        }
        Rectangle rectangle = (Rectangle) object;
        if(this.sideA != rectangle.sideA){
            return false;
        }
        return this.sideB == rectangle.sideB;
    }


    /**
     * Returns a hashcode of a rectangle (in this case, a square)
     *
     * @return int hashcode
     */
    @Override
    public int hashCode() {
        return (int)(15*sideA + 27*sideB);
    }
}