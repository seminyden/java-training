//  Данные для xml файла взяты из "Java. Методы программирования.", И.Н.Блинов, В.С.Романчик, стр.450, гл. 14, 1 вариант.
//
//
//  Оранжерея.
//        Растения, содержащиеся в оранжерее, имеют следующие характеристики:
//        — Name — название растения;
//        — Soil — почва для посадки, которая может быть следующих типов: подзолистая, грунтовая, дерново-подзолистая;
//        — Origin — место происхождения растения;
//        — Visual рarameters (должно быть несколько) — внешние параметры: цвет
//        стебля, цвет листьев, средний размер растения;
//        — Growing tips (должно быть несколько) — предпочтительные условия
//        произрастания: температура (в градусах), освещение (светолюбиво либо
//        нет), полив (мл в неделю);
//        — Multiplying — размножение: листьями, черенками либо семенами.
//


import com.greenhouse.*;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import static action.Marshal.marshalGreenhouse;
import static action.Unmarshal.unmarshalGreenhouse;
import static action.Validate.validate;

class Test {

    private static File xml = new File("files\\greenhouse.xml");
    private static File xsd = new File("files\\schema.xsd");

    public static void main(String[] args) {

        System.out.println("Данные для xml файла взяты из 'Java. Методы программирования.', И.Н.Блинов, В.С.Романчик, стр.450, гл. 14, 1 вариант." + "\n");

        Greenhouse greenhouse = null;

        //создаем оранжерею
        greenhouse = new Greenhouse();

        //добавляем цветы в оранжерею
        greenhouse.getFlower().add(createFlower("Rose",Soil.DERNOVO_PODZOLISTAYA,"Everywhere", 4,"green","green", 20,100,true));
        greenhouse.getFlower().add(createFlower("Chamomile",Soil.PODZOLISTAYA,"in temperate countries", 3,"green","green", 25,50,true));
        greenhouse.getFlower().add(createFlower("Chrysanthemum",Soil.GRUNTOVAYA,"warm", 10,"green","green", 15,125,true));


        //маршализация объекта оранжереи в xml файл
        try {
            System.out.println("Маршализация...");
            marshalGreenhouse(Greenhouse.class,greenhouse,xml);
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        greenhouse = null;
        System.out.println(greenhouse + "\n");


        //проверка корректности xml файла
        try {
            System.out.println("Проверка корректности xml файла...");
            validate(xml,xsd);
            System.out.println(xml + " is valid" + "\n");
        } catch (SAXException e) {
            System.err.println(xml +" not valid because " + e.getMessage() + "\n");
        } catch (IOException e) {
            System.err.println(xml + "not valid because " + e.getMessage() + "\n");
        }



        //демаршализация объекта
        try {
            System.out.println("Демаршализация...");
            greenhouse = unmarshalGreenhouse(Greenhouse.class,xml);
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println(greenhouse);

    }


    //создаем цветок, добавляем параметры

    public static Flower createFlower(String nameFlower,                    //название цветка
                               Soil soil,                                   //почва для посадки
                               String origin,                               //место происхождения растения
                               int averageSize,                             //средний размер растения
                               String leafColor,                            //цвет листьев
                               String stemColor,                            //цвет стебля
                               int temperature,                             //температура
                               int watering,                                //полив
                               boolean photophilous                         //светолюбив или нет
                               ){

        //создаем цветок добавляем параметры
        Flower flower = new Flower();
        flower.setName(nameFlower);
        flower.setSoil(soil);
        flower.setOrigin(origin);

        //создаем визуальные параметры
        VisualParameters vp = new VisualParameters();
        vp.setAverageSize(averageSize);
        vp.setLeafColor(leafColor);
        vp.setStemColor(stemColor);
        flower.setVisualParameters(vp);

        //создаем информацию об условиях произрастания
        GrowingTips gt = new GrowingTips();
        gt.setTemperature(temperature);
        gt.setWatering(watering);
        gt.setPhotophilous(photophilous);
        flower.setGrowingTips(gt);

        flower.setMultiplaying(Multiplying.SEED);
        return flower;
    }
}