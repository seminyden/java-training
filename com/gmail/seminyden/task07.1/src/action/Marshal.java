package action;

import com.greenhouse.Greenhouse;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public abstract class Marshal {

    //маршализация

    public static void marshalGreenhouse(Class c,                           //класс, объект которого маршализуем
                                         Greenhouse greenhouse,             //объект который записываем в файл
                                         File xml)                          //файл, в который записываем

            throws JAXBException, FileNotFoundException {

        JAXBContext context = JAXBContext.newInstance(c);
        Marshaller m = context.createMarshaller();
        m.marshal(greenhouse, new FileOutputStream(xml));
    }
}