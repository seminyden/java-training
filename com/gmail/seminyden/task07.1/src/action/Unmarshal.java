package action;

import com.greenhouse.Greenhouse;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public abstract class Unmarshal {

    //демаршализация

    public static Greenhouse unmarshalGreenhouse(Class c,                   //класс объекта, который демаршализуем
                                                 File xml)                  //файл, из которого получаем значения

            throws JAXBException, FileNotFoundException {

        JAXBContext context = JAXBContext.newInstance(c);
        Unmarshaller un = context.createUnmarshaller();
        Greenhouse greenhouse = (Greenhouse) un.unmarshal(new FileInputStream(xml));
        return greenhouse;
    }
}