<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         isELIgnored="false" pageEncoding="UTF-8"%>
<html>
<head>
    <title>${user.login}'s todolist</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
</head>
<body>
<div class="container">
    <table class="main">
        <tr>
            <td class="header" colspan="2">
                <%@include file="templates/header.jsp"%>
            </td>
        </tr>
        <tr>
            <td class="left_panel">
                <%@ include file="templates/menu.jsp"%>
            </td>
            <td class="content">
                <div>
                    <h2 class="page_title">${date}</h2>
                </div>
                <div>
                    <ul class="general_action">
                        <li><a href="${pageContext.request.contextPath}/form/create">+ todo</a></li>
                    </ul>
                </div>
                <c:forEach items="${todoList}" var="todo">
                <div>
                    <table class="todo">
                        <tr>
                            <td class="title">#${todo.id} ${todo.title}</td>
                            <td class="file"><a href="${pageContext.request.contextPath}/download?file=${todo.fileName}" class="flink">${todo.fileName}</a></td>
                            <td class="todo_action" rowspan="2">
                                <form action="${pageContext.request.contextPath}/form/edit" method="GET">
                                    <button type="submit" name="id" value="${todo.id}">edit</button>
                                </form>
                                <form action="${pageContext.request.contextPath}/todo/delete" method="POST">
                                    <button type="submit" name="id" value="${todo.id}">delete</button>
                                </form>
                                <form action="${pageContext.request.contextPath}/todo/done" method="POST">
                                    <button type="submit" name="id" value="${todo.id}">done</button>
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" colspan="2">${todo.text}</td>
                        </tr>
                    </table>
                </div>
                </c:forEach>
            </td>
        </tr>
        <tr>
            <td class="footer" colspan="2">
                <%@include file="templates/footer.jsp"%>
            </td>
        </tr>
    </table>
</div>

</body>
</html>