<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         isErrorPage="true" isELIgnored="false" %>
<html>
<head>
    <title>Error</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
</head>
<body>
<div class="container">
    <table class="err">
        <tr>
            <td><p><b>Error:</b></p></td>
            <td>${pageContext.exception}</td>
        </tr>
        <tr>
            <td><p><b>Status code:</b></p></td>
            <td>${pageContext.errorData.statusCode}</td>
        </tr>
        <tr>
            <td class="stack_trace"><p><b>Stack trace:</b></p></td>
            <td>
                <c:forEach items="${pageContext.exception.stackTrace}" var="trace">
                    <p>${trace}</p>
                </c:forEach>
            </td>
        </tr>
    </table>
</div>
</body>
</html>