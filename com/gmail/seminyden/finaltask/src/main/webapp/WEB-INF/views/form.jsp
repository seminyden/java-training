<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         isELIgnored="false" pageEncoding="UTF-8"%>
<html>
<head>
    <title>${user.login}'s todolist</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
</head>
<body>
<div class="container">
    <table class="main">
        <tr>
            <td class="header" colspan="2">
                <%@include file="templates/header.jsp"%>
            </td>
        </tr>
        <tr>
            <td class="left_panel">
                <%@ include file="templates/menu.jsp"%>
            </td>
            <td class="content">
                <div>
                    <c:if test="${toDo == null}"><h2 class="page_title">Create todo</h2></c:if>
                    <c:if test="${toDo != null}"><h2 class="page_title">Edit todo</h2></c:if>
                </div>
                <c:if test="${toDo != null && toDo.fileName != null && !toDo.fileName.isEmpty()}">
                <div>
                    <form action="${pageContext.request.contextPath}/form/unload" method="POST">
                        <input type="hidden" name="id" value="${toDo.id}">
                        <table class="edit_form">
                            <tr>
                                <td><a href="${pageContext.request.contextPath}/download?file=${toDo.fileName}" class="flink">${toDo.fileName}</a></td>
                                <td class="unload"><button type="submit">Unload</button></td>
                            </tr>
                        </table>
                    </form>
                </div>
                </c:if>
                <div>
                    <c:if test="${toDo == null}"><form action="${pageContext.request.contextPath}/form/create" method="POST" enctype="multipart/form-data"></c:if>
                    <c:if test="${toDo != null}"><form action="${pageContext.request.contextPath}/form/edit" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="${toDo.id}">
                            </c:if>
                        <table class="edit_form">
                            <tr>
                                <td><p>Title:</p></td>
                                <c:if test="${toDo == null}"><td><input type="text" name="title" placeholder="Title"></td></c:if>
                                <c:if test="${toDo != null}"><td><input type="text" name="title" value="${toDo.title}"></td></c:if>
                            </tr>
                            <tr>
                                <td><p>Text:</p></td>
                                <c:if test="${toDo == null}"><td><textarea name="text" placxeholder="Text"></textarea></td></c:if>
                                <c:if test="${toDo != null}"><td><textarea name="text">${toDo.text}</textarea></td></c:if>
                            </tr>
                            <tr>
                                <td><p>File:</p></td>
                               <td><input type="file" name="file"></td>
                            </tr>
                            <tr>
                                <td><p>Date:</p></td>
                                <c:if test="${toDo == null}"><td><input type="date" name="date"></td></c:if>
                                <c:if test="${toDo != null}"><td><input type="date" name="date" value="${toDo.dueDate}"></td></c:if>
                            </tr>
                            <tr>
                                <td colspan="2" class="submit">
                                    <c:if test="${toDo == null}">
                                        <button type="submit">Create</button>
                                        <button type="reset">Clear</button>
                                    </c:if>
                                    <c:if test="${toDo != null}"><button type="submit">Save</button></c:if>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </td>
        </tr>
        <tr>
            <td class="footer" colspan="2">
                <%@include file="templates/footer.jsp"%>
            </td>
        </tr>
    </table>
</div>

</body>
</html>