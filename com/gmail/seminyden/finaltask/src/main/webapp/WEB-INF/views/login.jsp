<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
</head>
<body>
    <div class="container">
        <table class="user_form">
            <form action="${pageContext.request.contextPath}/login" method="POST">
                <caption><h2>Login to account</h2></caption>
                <c:if test="${message != null}"><tr><td colspan="2" class="message">${message}</td></tr></c:if>
                <tr>
                    <td><p>Login:</p></td>
                    <td><input type="text" name="login" placeholder="Login"></td>
                </tr>
                <tr>
                    <td><p>Password:</p></td>
                    <td><input type="password" name="password" placeholder="Password"></td>
                </tr>
                <tr>
                    <td class="submit" colspan="2"><button type="submit">Login</button></td>
                </tr>
            </form>
        </table>
    </div>
    <div class="links">
        <a href="${pageContext.request.contextPath}/registration">Create new account</a>
    </div>
</body>
</html>