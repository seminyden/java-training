<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         isELIgnored="false" pageEncoding="UTF-8"%>
<html>
<head>
    <title>Recycle</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
</head>
<body>
<div class="container">
    <table class="main">
        <tr>
            <td class="header" colspan="2">
                <%@include file="templates/header.jsp"%>
            </td>
        </tr>
        <tr>
            <td class="left_panel">
                <%@ include file="templates/menu.jsp"%>
            </td>
            <td class="content">
                <div>
                    <h2 class="page_title">Recycle</h2>
                </div>
                <form action="${pageContext.request.contextPath}/recycle/" method="POST">
                    <div>
                        <ul class="general_action">
                            <li><button type="submit" name="action" value="restore">restore</button></li>
                            <li><button type="submit" name="action" value="remove">remove</button></li>
                        </ul>
                    </div>
                    <c:forEach items="${todoList}" var="todo">
                        <div>
                            <table class="todo">
                                <tr>
                                    <td class="title">
                                        <input type="checkbox" name="id" value="${todo.id}">
                                        #${todo.id} ${todo.title}
                                    </td>
                                    <td class="file">${todo.fileName}</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="2">${todo.text}</td>
                                </tr>
                            </table>
                        </div>
                    </c:forEach>
                </form>
            </td>
        </tr>
        <tr>
            <td class="footer" colspan="2">
                <%@include file="templates/footer.jsp"%>
            </td>
        </tr>
    </table>
</div>
</body>
</html>