package app.servlets.form.action;

import app.entities.User;
import app.entities.todo.ToDo;
import app.services.ToDoServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;

@WebServlet("/form/edit")
@MultipartConfig
public class EditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int todoId = Integer.parseInt(req.getParameter("id"));

        ToDoServiceImpl service = new ToDoServiceImpl();

        ToDo toDo = service.getToDo(todoId);

        req.setAttribute("toDo",toDo);
        req.getRequestDispatcher("/form").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");

        int todoId = Integer.parseInt(req.getParameter("id"));

        String title = req.getParameter("title");
        String text = req.getParameter("text");
        Part filePart = req.getPart("file");
        String date = req.getParameter("date");

        ToDoServiceImpl service = new ToDoServiceImpl();

        ToDo toDo = service.getToDo(todoId);

        String todoFileName = toDo.getFileName();

        if(!filePart.getSubmittedFileName().isEmpty() && filePart.getSize() != 0){
            if(todoFileName != null && !todoFileName.isEmpty()){

                if(service.unloadFile(user.getId(),todoFileName)){
                    File file = service.uploadFile(user.getId(),filePart);

                    if(file != null) {
                        toDo.setFileName(file.getName());
                    }
                }

            } else {
                File file = service.uploadFile(user.getId(),filePart);

                if(file != null) {
                    toDo.setFileName(file.getName());
                }
            }
        }

        toDo.setTitle(title);
        toDo.setText(text);
        toDo.setDueDate(date);
        service.updateToDo(toDo);

        resp.sendRedirect(req.getContextPath() + "/todo?day=someday&date=" + date);
    }
}