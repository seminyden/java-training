package app.services;

import app.entities.User;

public interface UserService {

    boolean createUser(String login, String password, String email);

    User getUser(String login, String password);

    boolean checkLogin(String login);

    boolean updateUser(User user);

    boolean deleteUser(int userId);
}