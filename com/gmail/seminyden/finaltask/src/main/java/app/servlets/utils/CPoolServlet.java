package app.servlets.utils;

import app.utils.cpool.ConnectionPool;
import app.utils.cpool.exception.ConnectionPoolException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet(value = "/cpool", loadOnStartup = 0)
public class CPoolServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        try {
            ConnectionPool.getInstance().initPoolData();
            System.out.println("ConnectionPool init");
        } catch (ConnectionPoolException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        ConnectionPool.getInstance().dispose();
        System.out.println("ConnectionPool close");
    }
}
