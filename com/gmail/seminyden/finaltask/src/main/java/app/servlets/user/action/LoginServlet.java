package app.servlets.user.action;

import app.entities.User;
import app.services.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/views/login.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        UserServiceImpl service = new UserServiceImpl();
        User user = service.getUser(login, password);

        if(user != null){
            req.getSession().setAttribute("user",user);
            resp.sendRedirect(req.getContextPath() + "/todo?day=today&date=");

        } else {
            String message = "<p class='denied'>Incorrect login or password!</p>";
            req.setAttribute("message",message);
            req.getRequestDispatcher("/WEB-INF/views/login.jsp").forward(req,resp); }
    }
}