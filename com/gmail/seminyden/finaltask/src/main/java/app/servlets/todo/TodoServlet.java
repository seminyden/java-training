package app.servlets.todo;

import app.entities.User;
import app.entities.todo.Status;
import app.entities.todo.ToDo;
import app.services.ToDoServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@WebServlet("/todo/*")
public class TodoServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");
        String day = req.getParameter("day");

        if(day == null || day.trim().isEmpty()){
            day = "today";
        }

        String date = null;

        switch (day.trim()){
            case "today":
                date = LocalDate.now().toString();
                break;

            case "tomorrow":
                date = LocalDate.now().plusDays(1).toString();
                break;

            case "someday":
                date = req.getParameter("date");
                if(date == null || date.trim().isEmpty()){
                    date = LocalDate.now().toString();
                }
                break;
        }

        ToDoServiceImpl service = new ToDoServiceImpl();
        List<ToDo> todoList = service.todoListByDate(user.getId(),date, Status.ACTUAL);

        req.setAttribute("date",date);
        req.setAttribute("todoList",todoList);
        req.getRequestDispatcher("/WEB-INF/views/todo.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
