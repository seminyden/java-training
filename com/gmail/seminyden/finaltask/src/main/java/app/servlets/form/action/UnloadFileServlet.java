package app.servlets.form.action;

import app.entities.User;
import app.entities.todo.ToDo;
import app.services.ToDoServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/form/unload")
public class UnloadFileServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/form").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");

        int todoId = Integer.parseInt(req.getParameter("id"));

        ToDoServiceImpl service = new ToDoServiceImpl();

        ToDo toDo = service.getToDo(todoId);

        if(service.unloadFile(user.getId(),toDo.getFileName())){
            toDo.setFileName("");
            service.updateToDo(toDo);
        }

        resp.sendRedirect( req.getContextPath() + "/form/edit?id=" + todoId);
    }
}