package app.services;

import app.entities.todo.Status;
import app.entities.todo.ToDo;
import app.utils.cpool.ConnectionPool;
import app.utils.cpool.exception.ConnectionPoolException;

import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ToDoServiceImpl implements ToDoService {

    public static final String UPLOAD_PATH = "D://uploads/";

    @Override
    public boolean createToDo(int userId, String title, String text, File file, String dueDate) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement createToDo = null;
        boolean result = false;

        if(userId <= 0 || text == null || text.trim().isEmpty()){
            return result;
        }

        title = title != null ? title.trim() : "";

        if(dueDate == null || dueDate.trim().isEmpty()){
            dueDate = LocalDate.now().toString();
        }

        String fileName = file != null ? file.getName() : "";

        try {
            connection = connectionPool.takeConnection();
            createToDo = connection.prepareStatement("INSERT INTO toDoList(u_id,todo_title,todo_text,due_date,file_name) VALUE(?,?,?,?,?)");
            createToDo.setInt(1,userId);
            createToDo.setString(2,title.trim());
            createToDo.setString(3,text.trim());
            createToDo.setString(4,dueDate.trim());
            createToDo.setString(5,fileName);

            result = createToDo.executeUpdate() > 0;

        } catch (ConnectionPoolException | SQLException e) {
            e.printStackTrace();

        } finally {
            connectionPool.closeConnection(connection,createToDo);
        }

        return result;
    }

    @Override
    public ToDo getToDo(int todoId) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement getToDo = null;
        ResultSet result = null;
        ToDo toDo = null;

        if(todoId <= 0){
            return toDo;
        }

        try {
            connection = connectionPool.takeConnection();
            getToDo = connection.prepareStatement("SELECT * FROM toDoList WHERE todo_id = ?");//
            getToDo.setInt(1,todoId);

            result = getToDo.executeQuery();

            if(result.next()){
                toDo = new ToDo(result.getInt("todo_id"),
                                result.getInt("u_id"),
                                result.getString("todo_title"),
                                result.getString("todo_text"),
                                result.getString("due_date"),
                                result.getString("file_name"),
                                Status.valueOf(result.getString("status")));
            }

        } catch (ConnectionPoolException | SQLException e) {
            e.printStackTrace();

        } finally {
            connectionPool.closeConnection(connection,getToDo,result);
        }

        return toDo;
    }

    @Override
    public List<ToDo> todoListByDate(int userId, String dueDate, Enum status) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement getList = null;
        ResultSet result = null;
        List<ToDo> toDoList = new ArrayList<ToDo>();

        if(userId <= 0 || dueDate == null || dueDate.trim().isEmpty() || status == null){
            return toDoList;
        }

        try {
            connection = connectionPool.takeConnection();
            getList = connection.prepareStatement("SELECT * FROM toDoList WHERE u_id = ? and due_date = ? and status = ?");//
            getList.setInt(1,userId);
            getList.setString(2,dueDate.trim());
            getList.setString(3,status.toString());

            result = getList.executeQuery();

            while(result.next()){
                toDoList.add(new ToDo(result.getInt("todo_id"),
                                      result.getInt("u_id"),
                                      result.getString("todo_title"),
                                      result.getString("todo_text"),
                                      result.getString("due_date"),
                                      result.getString("file_name"),
                                      Status.valueOf(result.getString("status"))));
            }

        } catch (ConnectionPoolException | SQLException e) {
            e.printStackTrace();

        } finally {
            connectionPool.closeConnection(connection,getList,result);
        }

        return toDoList;
    }

    @Override
    public List<ToDo> todoList(int userId, Enum status) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement getList = null;
        ResultSet result = null;
        List<ToDo> toDoList = new ArrayList<ToDo>();

        if(userId <= 0 || status == null){
            return toDoList;
        }

        try {
            connection = connectionPool.takeConnection();
            getList = connection.prepareStatement("SELECT * FROM toDoList WHERE u_id = ? and status = ?");//
            getList.setInt(1,userId);
            getList.setString(2,status.toString());

            result = getList.executeQuery();

            while(result.next()){
                toDoList.add(new ToDo(result.getInt("todo_id"),
                                      result.getInt("u_id"),
                                      result.getString("todo_title"),
                                      result.getString("todo_text"),
                                      result.getString("due_date"),
                                      result.getString("file_name"),
                                      Status.valueOf(result.getString("status"))));
            }

        } catch (ConnectionPoolException | SQLException e) {
            e.printStackTrace();

        } finally {
            connectionPool.closeConnection(connection,getList,result);
        }

        return toDoList;
    }

    @Override
    public boolean updateToDo(ToDo toDo) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement updateToDo = null;
        boolean result = false;

        if(toDo == null){
            return result;
        }

        try {
            connection = connectionPool.takeConnection();
            updateToDo = connection.prepareStatement("UPDATE toDoList SET todo_title=?, todo_text=?, due_date=?, file_name=?, status=? WHERE todo_id = ?");//
            updateToDo.setString(1,toDo.getTitle());
            updateToDo.setString(2,toDo.getText());
            updateToDo.setString(3,toDo.getDueDate());
            updateToDo.setString(4,toDo.getFileName());
            updateToDo.setString(5,toDo.getStatus().toString());
            updateToDo.setInt(6,toDo.getId());

            result = updateToDo.executeUpdate() > 0;

        } catch (ConnectionPoolException | SQLException e) {
            e.printStackTrace();

        } finally {
            connectionPool.closeConnection(connection,updateToDo);
        }

        return result;
    }

    @Override
    public File uploadFile(int userId, Part filePart) throws IOException {      //осуществить разделение одинаковых файлов
        File file = null;

        if(userId <= 0 || filePart == null){
            return file;
        }

        String fileName = filePart.getSubmittedFileName();
        if(fileName.isEmpty()){
            return file;
        }

        File uploadDir = new File(UPLOAD_PATH);
        if (!uploadDir.exists()){
            uploadDir.mkdir();
        }

        File userDir = new File(UPLOAD_PATH + userId);
        if (!userDir.exists()){
            userDir.mkdir();
        }

        InputStream fileContent = filePart.getInputStream();

        file = new File(userDir,fileName);
        Files.copy(fileContent,file.toPath(), StandardCopyOption.REPLACE_EXISTING);

        fileContent.close();

        return file;
    }

    @Override
    public boolean unloadFile(int userId, String fileName) {    //тоже подумать о разделении одинаковых файлов
        boolean result = false;

        if(userId <=0 || fileName == null || fileName.isEmpty()){
            return result;
        }

        File file = new File(UPLOAD_PATH + userId + "/" + fileName);

        if(file.exists()){
            result = file.delete();
        }

        return result;
    }

    @Override
    public boolean deleteToDo(int toDoId) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement deleteToDo = null;
        boolean result = false;

        if(toDoId <= 0){
            return result;
        }

        try {
            connection = connectionPool.takeConnection();
            deleteToDo = connection.prepareStatement("DELETE FROM toDoList WHERE todo_id = ?");//
            deleteToDo.setInt(1,toDoId);

            result = deleteToDo.executeUpdate() > 0;

        } catch (ConnectionPoolException | SQLException e) {
            e.printStackTrace();

        } finally {
            connectionPool.closeConnection(connection,deleteToDo);
        }

        return result;
    }
}