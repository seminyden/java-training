package app.servlets.form.action;

import app.entities.User;
import app.services.ToDoServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;

@WebServlet("/form/create")
@MultipartConfig
public class CreateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/form").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");

        String title = req.getParameter("title");
        String text = req.getParameter("text");
        Part filePart = req.getPart("file");
        String date = req.getParameter("date");

        ToDoServiceImpl service = new ToDoServiceImpl();

        File file = service.uploadFile(user.getId(),filePart);

        service.createToDo(user.getId(),title,text,file,date);

        resp.sendRedirect(req.getContextPath() + "/todo?day=someday&date=" + date);
    }
}