package app.services;

import app.entities.todo.ToDo;

import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.util.List;

public interface ToDoService {

    boolean createToDo(int userId, String title, String text, File file, String dueDate);

    ToDo getToDo(int id);

    List<ToDo> todoListByDate(int userId, String dueDate, Enum status);

    List<ToDo> todoList(int userId, Enum status);

    boolean updateToDo(ToDo note);

    File uploadFile(int userId, Part filePart) throws IOException;

    boolean unloadFile(int userId, String fileName);

    boolean deleteToDo(int noteId);
}