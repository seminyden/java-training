package app.servlets.user.action;

import app.services.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/views/registration.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String email = req.getParameter("email");

        UserServiceImpl service = new UserServiceImpl();

        if(service.checkLogin(login)){
            String message = "<p class='denied'>This login already registered!</p>";
            req.setAttribute("message",message);
            req.getRequestDispatcher("/WEB-INF/views/registration.jsp").forward(req,resp);

        } else {

            if(service.createUser(login,password,email)){
                String message = "<p class='accept'>User '" + login + "' successfully registered!</p>";
                req.setAttribute("message",message);
                req.getRequestDispatcher("/WEB-INF/views/login.jsp").forward(req,resp);

            } else {
                String message = "<p class='denied'>Registration fail.</p>";
                req.setAttribute("message",message);
                req.getRequestDispatcher("/WEB-INF/views/registration.jsp").forward(req,resp);
            }
        }
    }
}