package app.servlets.recycle.action;

import app.entities.todo.Status;
import app.entities.todo.ToDo;
import app.services.ToDoServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet("/recycle/restore")
public class RestoreServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/recycle").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] idArr = req.getParameterValues("id");

        ToDoServiceImpl service = new ToDoServiceImpl();

        if(idArr == null){
            idArr = new String[]{};
        }

        for(String id : idArr){
            int todoId = Integer.parseInt(id);
            ToDo toDo = service.getToDo(todoId);
            toDo.setDueDate(LocalDate.now().toString());
            toDo.setStatus(Status.ACTUAL);
            service.updateToDo(toDo);
        }

        resp.sendRedirect(req.getContextPath() + "/recycle");
    }
}