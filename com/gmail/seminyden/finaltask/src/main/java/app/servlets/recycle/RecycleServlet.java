package app.servlets.recycle;

import app.entities.User;
import app.entities.todo.Status;
import app.entities.todo.ToDo;
import app.services.ToDoServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/recycle/*")
public class RecycleServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");

        ToDoServiceImpl service = new ToDoServiceImpl();
        List<ToDo> todoList = service.todoList(user.getId(), Status.RECYCLE);

        req.setAttribute("todoList",todoList);
        req.getRequestDispatcher("/WEB-INF/views/recycle.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");

        if(action == null){
            action = "";
        }

        switch (action){
            case "restore":
                req.getRequestDispatcher("/recycle/restore").forward(req,resp);
                break;

            case "remove":
                req.getRequestDispatcher("/recycle/remove").forward(req,resp);
                break;

            default:
                doGet(req, resp);
                break;
        }
    }
}