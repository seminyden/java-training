package app.servlets.todo.action;

import app.entities.todo.Status;
import app.entities.todo.ToDo;
import app.services.ToDoServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/todo/delete")
public class DeleteServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/todo").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int todoId = Integer.parseInt(req.getParameter("id"));     //вызывает исключение в случае неправильного формата числа

        ToDoServiceImpl service = new ToDoServiceImpl();

        ToDo toDo = service.getToDo(todoId);

        String date = "";

        if(toDo != null) {
            toDo.setStatus(Status.RECYCLE);
            date = toDo.getDueDate();
        }

        service.updateToDo(toDo);

        resp.sendRedirect(req.getContextPath() + "/todo?day=someday&date=" + date);
    }
}