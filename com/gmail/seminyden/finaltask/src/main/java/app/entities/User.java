package app.entities;

public class User {

    private int id;
    private String login;
    private String password;
    private String email;

    public User(){}

    public User(int id,
                String login,
                String password,
                String email) {

        this.id = id;
        this.login = login;
        this.password = password;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int l = login != null ? login.hashCode() : 0;
        int p = password != null ? password.hashCode() : 0;
        int e = email != null ? email.hashCode() : 0;
        return id + 15*l + 21*p + 3*e;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
               login.equals(user.login) &&
               password.equals(user.password) &&
               email.equals(user.email);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " #" + id + ", login: " + login + ", email: " + email;
    }
}