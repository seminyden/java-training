package app.entities.todo;

public class ToDo {

    private int id;
    private int userId;
    private String title;
    private String text;
    private String dueDate;
    private String fileName;
    private Enum status;

    public ToDo(){}

    public ToDo(int id,
                int userId,
                String title,
                String text,
                String dueDate,
                String fileName,
                Enum status) {

        setId(id);
        setUserId(userId);
        setTitle(title);
        setText(text);
        setDueDate(dueDate);
        setFileName(fileName);
        setStatus(status);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Enum getStatus() {
        return status;
    }

    public void setStatus(Enum status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int tt = title != null ? title.hashCode() : 0;
        int tx = text != null ? text.hashCode() : 0;
        int d = dueDate != null ? dueDate.hashCode() : 0;
        int f = fileName != null ? fileName.hashCode() : 0;
        int s = status != null ? status.hashCode() : 0;
        return id + userId + 11*tt + 9*tx + 7*d + 5*f + 3*s;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ToDo toDo = (ToDo) o;
        return id == toDo.id &&
               userId == toDo.userId &&
               title.equals(toDo.title) &&
               text.equals(toDo.text) &&
               dueDate.equals(toDo.dueDate) &&
               fileName.equals(toDo.fileName) &&
               status.equals(toDo.status);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " #" + id + " by user #" + userId + "\n" +
                "status: " + status + "\n" +
                "dueDate: " + dueDate + "\n" +
                "title: " + title + "\n" +
                "text: " + text + "\n" +
                "file: " + fileName  + "\n";
    }
}