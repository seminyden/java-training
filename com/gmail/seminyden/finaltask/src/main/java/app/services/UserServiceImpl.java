package app.services;

import app.entities.User;
import app.utils.cpool.ConnectionPool;
import app.utils.cpool.exception.ConnectionPoolException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserServiceImpl implements UserService {

    @Override
    public boolean createUser(String login, String password, String email) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement createUser = null;
        boolean result = false;

        if (login == null || password == null || login.trim().isEmpty() || password.isEmpty()) {
            return result;
        }

        try {
            connection = connectionPool.takeConnection();
            createUser = connection.prepareStatement("INSERT INTO users VALUE(NULL,?,?,?)");//
            createUser.setString(1, login.trim());
            createUser.setString(2, password);
            createUser.setString(3, email);

            result = createUser.executeUpdate() > 0;

        } catch (ConnectionPoolException | SQLException e) {
            e.printStackTrace();

        } finally {
            connectionPool.closeConnection(connection, createUser);
        }

        return result;
    }

    @Override
    public User getUser(String login, String password) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement getUser = null;
        ResultSet result = null;
        User user = null;

        if (login == null || password == null || login.trim().isEmpty() || password.isEmpty()) {
            return user;
        }

        try {
            connection = connectionPool.takeConnection();
            getUser = connection.prepareStatement("SELECT * FROM users WHERE login = ? and password = ?");//
            getUser.setString(1, login.trim());
            getUser.setString(2, password);

            result = getUser.executeQuery();

            if (result.next()) {
                user = new User(result.getInt("id"),
                        result.getString("login"),
                        result.getString("password"),
                        result.getString("email"));
            }

        } catch (ConnectionPoolException | SQLException e) {
            e.printStackTrace();

        } finally {
            connectionPool.closeConnection(connection, getUser, result);
        }

        return user;
    }

    @Override
    public boolean checkLogin(String login) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement checkLogin = null;
        boolean result = false;

        if (login == null || login.trim().isEmpty()) {
            return result;
        }

        try {
            connection = connectionPool.takeConnection();
            checkLogin = connection.prepareStatement("SELECT login FROM users WHERE login = ?");//
            checkLogin.setString(1, login.trim());

            result = checkLogin.executeQuery().next();

        } catch (ConnectionPoolException | SQLException e) {
            e.printStackTrace();

        } finally {
            connectionPool.closeConnection(connection, checkLogin);
        }

        return result;
    }

    @Override
    public boolean updateUser(User user) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement updateUser = null;
        boolean result = false;

        if (user == null) {
            return result;
        }

        try {
            connection = connectionPool.takeConnection();
            updateUser = connection.prepareStatement("UPDATE users SET login=?, password=?, email=? WHERE id=?");//
            updateUser.setString(1, user.getLogin());
            updateUser.setString(2, user.getPassword());
            updateUser.setString(3, user.getEmail());
            updateUser.setInt(4, user.getId());

            result = updateUser.executeUpdate() > 0;

        } catch (ConnectionPoolException | SQLException e) {
            e.printStackTrace();

        } finally {
            connectionPool.closeConnection(connection, updateUser);
        }

        return result;
    }

    @Override
    public boolean deleteUser(int userId) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement deleteUser = null;
        boolean result = false;

        if (userId <= 0) {
            return result;
        }

        try {
            connection = connectionPool.takeConnection();
            deleteUser = connection.prepareStatement("DELETE FROM users WHERE id = ?");//
            deleteUser.setInt(1, userId);

            result = deleteUser.executeUpdate() > 0;

        } catch (ConnectionPoolException | SQLException e) {
            e.printStackTrace();

        } finally {
            connectionPool.closeConnection(connection, deleteUser);
        }

        return result;
    }
}