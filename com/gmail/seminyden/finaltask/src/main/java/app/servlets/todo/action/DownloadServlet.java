package app.servlets.todo.action;

import app.entities.User;
import app.services.ToDoServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@WebServlet("/download")
public class DownloadServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");
        String fileName = req.getParameter("file");

        String filePath = ToDoServiceImpl.UPLOAD_PATH + user.getId() + "/" + fileName;

        File file = new File(filePath);

        FileInputStream fileIn = new FileInputStream(file);
        BufferedInputStream BufferedIn = new BufferedInputStream(fileIn);

        int size = (int) file.length();
        byte[] data = new byte[size];

        BufferedIn.read(data,0,size);
        BufferedIn.close();

        Path path = Paths.get(filePath);
        String contentType = Files.probeContentType(path);

        resp.setContentType(contentType);
        resp.setHeader("Content-Disposition","attachment; filename=\"" + file.getName() + "\"" );

        ServletOutputStream out = resp.getOutputStream();
        out.write(data);
        out.flush();
        out.close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
