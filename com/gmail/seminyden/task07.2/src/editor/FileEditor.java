package editor;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileEditor {

    private DocumentBuilder builder;
    private Transformer transformer;

    private FileEditor(){}

    private static FileEditor instance = new FileEditor();

    public static FileEditor getInstance(DocumentBuilder builder,
                                  Transformer transformer){

        instance.builder = builder;
        instance.transformer = transformer;
        return instance;
    }

    //добавить префикс к файлу

    public File addPrefix(File file,                //файл
                          int offset,               //позиция префикса
                          String prefix){           //префикс

        String path = file.getParentFile().getPath() + "\\";
        StringBuilder name = new StringBuilder(file.getName());
        name.insert(offset,prefix);
        File temp = new File(path + name);
        file.renameTo(temp);
        return temp;
    }

    //установить атрибут

    public Document setAttribute(Document document,         //документ
                                 String tagName,            //название тега
                                 String attrName,           //название атрибута
                                 String attrValue)          //новое значение атрибута

            throws IOException, SAXException {

        Element root = document.getDocumentElement();
        Node tag = root.getElementsByTagName(tagName).item(0);
        ((Element) tag).setAttribute(attrName,attrValue);
        return document;
    }

    //сохранение изменений контента

    public void saveFileContents(Document document,         //измененный документ
                                 String path)               //путь к документу

            throws IOException, TransformerException {

        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(new FileWriter(path));
        transformer.transform(source,result);
    }
}
