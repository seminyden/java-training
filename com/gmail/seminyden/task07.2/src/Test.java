import editor.FileEditor;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;

public class Test {

    private static DocumentBuilder builder;
    private static Transformer transformer;
    private static FileEditor editor;

    private static int filesCount = 0;
    private static int dirCount = 0;

    private static File file = new File("ADC");
    private static final int FILE_OFFSET = 22;
    private static final int DIR_OFFSET = 0;
    private static final String PREFIX = "ADC_";
    private static final String NEW_ATTR_VALUE = "VALUE";

    public static void main(String[] args) throws ParserConfigurationException, TransformerException, IOException, SAXException {
        builder = createBuilder();
        transformer = createTransformer();
        editor = FileEditor.getInstance(builder,transformer);

        addPrefixToAll(file);
        filesCount = 0;
        changeFilesContents(file);
    }

    //получение DocumentBuilder

    public static DocumentBuilder createBuilder() throws ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder;
    }

    //получение Transformer

    public static Transformer createTransformer() throws TransformerConfigurationException {
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        return transformer;
    }

    //изменение значений атрибутов файла с сохранением

    public static void changeFileContents(File file) throws IOException, TransformerException, SAXException {

        Document document = builder.parse(file);
        editor.setAttribute(document,"PointOfSale","ParentPointOfSale", NEW_ATTR_VALUE);
        editor.setAttribute(document,"PointOfSale","PointOfSaleCode", NEW_ATTR_VALUE);
        editor.setAttribute(document,"PointOfSaleDescription","Description", NEW_ATTR_VALUE);
        editor.saveFileContents(document,file.getPath());
    }

    //изменение атрибутов во всех файлах в директории

    public static void changeFilesContents(File file) throws TransformerException, IOException, SAXException {

        if(file.isDirectory()){
            File[] childFiles = file.listFiles();
            for(File child : childFiles){
                if(child.isDirectory()){
                    changeFilesContents(child);
                }
                if(child.isFile() && !child.getParentFile().getName().equalsIgnoreCase("ADC")){
                    changeFileContents(child);
                    System.out.println("content file:" + ++filesCount);
                }
            }
        }
    }

    //добавить префикс ко всем файлам в директории

    public static void addPrefixToAll(File file){

        if(file.isDirectory()){
            File[] childFiles = file.listFiles();
            for(File child : childFiles){
                if(child.isDirectory()){
                    addPrefixToAll(child);
                    editor.addPrefix(child,DIR_OFFSET,PREFIX);
                    System.out.println("prefix dir:" + ++dirCount);
                }
                if(child.isFile() && !child.getParentFile().getName().equalsIgnoreCase("ADC")){
                    editor.addPrefix(child,FILE_OFFSET,PREFIX);
                    System.out.println("prefix files:" + ++filesCount);
                }
            }
        }
    }
}