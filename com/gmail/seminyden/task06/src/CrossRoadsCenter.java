import java.awt.*;

public class CrossRoadsCenter {

    private int id;
    private Color color;
    private volatile int countIn;

    private static Rectangle rect = new Rectangle(Settings.POINT_CROSS_ROAD.x, Settings.POINT_CROSS_ROAD.y,
            Settings.DIMENSION_CROSS_ROAD.width + 1, Settings.DIMENSION_CROSS_ROAD.height + 1);

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return id;
    }

    public void setColor(Color color){
        this.color = color;
    }

    public Color getColor(){
        return color;
    }

    public synchronized void incCountIn(){
        countIn = countIn + 1;
    }

    public synchronized void decCountIn(){
        countIn = countIn - 1;
    }

    public synchronized int getCountIn(){
        return countIn;
    }

    public boolean contains(int x, int y){
        return rect.contains(x,y);
    }

    public int getX(){
        return rect.x;
    }

    public int getY(){
        return rect.y;
    }

    public int getWidth(){
        return rect.width;
    }

    public int getHeight(){
        return rect.height;
    }
}
