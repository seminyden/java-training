import javax.swing.*;
import java.awt.*;
import java.util.Random;

/**
 * Created b8y vcnuv on 07.06.2018.
 */
public class ThreadSquare implements Runnable{
    private final boolean horVert;
    private int x, y;
    private int direction = 1;
    JButton button;
    private final Container contentPane;

    private CrossRoadsCenter center;
    private int id;
    private boolean inCenter;


    public ThreadSquare(boolean horVert, CrossRoadsCenter center){
        this.horVert = horVert;
        this.center = center;
        if(horVert){
            x = 0;
            y = Settings.POINT_CROSS_ROAD.y + (int)(Math.random() * (Settings.DIMENSION_CROSS_ROAD.height - Settings.HEIGHT_SQUARE));
        } else {
            x = Settings.POINT_CROSS_ROAD.x + (int)(Math.random() * (Settings.DIMENSION_CROSS_ROAD.width - Settings.WIDTH_SQUARE));
            y = 0;
        }
        this.contentPane = Settings.contentPane;
        initButton();
    }

    @Override
    public void run(){
            if (!Settings.numberTask) {
                firstOpt();
            } else {
                secondOpt();
            }
    }

    public void firstOpt(){
        while(true) {
            synchronized (center) {
                if (!Settings.numberTask) {
                    if (center.contains(x, y) || center.contains(x + Settings.WIDTH_SQUARE - 1, y + Settings.HEIGHT_SQUARE - 1)) {
                        synchronized (center) {
                            if (center.getId() > 0 && center.getId() != id) {
                                try {
                                    center.wait();
                                } catch (InterruptedException ex) {
                                    ex.printStackTrace();
                                }
                            } else {
                                center.setId(id);
                            }
                        }
                    } else if (center.getId() == id) {
                        synchronized (center) {
                            center.setId(0);
                            center.notify();
                        }
                    }
                }
            }
        move();
        getNextPoint();
        }
    }

    public void secondOpt(){
        while (true) {
            synchronized (center) {
                if (center.contains(x, y) || center.contains(x + Settings.WIDTH_SQUARE - 1, y + Settings.HEIGHT_SQUARE - 1)) {

                    if (center.getColor() == null) {
                        center.setColor(button.getBackground());
                    }

                    if (!center.getColor().equals(button.getBackground())) {
                        System.out.println(id + " wait");
                        try {
                            center.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        continue;

                    } else {
                        if (!inCenter) {
                            inCenter = true;
                            center.incCountIn();
                        }
                    }

                } else if (inCenter) {
                    inCenter = false;
                    center.decCountIn();
                }

                if (center.getCountIn() == 0) {
                    center.setColor(null);
                    center.notifyAll();
                }

            }
            move();
            getNextPoint();
        }
    }

    private void getNextPoint(){
        if(horVert){
            x += Settings.STEP_SQUARE * direction;
            if(x >= Settings.FRAME_WIDTH - Settings.WIDTH_SQUARE){
                direction = -1;
            }
            if(x<=0){
                direction = 1;
            }
        } else {
            y += Settings.STEP_SQUARE * direction;
            if(y>= Settings.FRAME_HEIGHT - Settings.HEIGHT_SQUARE - 24){
                direction = -1;
            }
            if(y<=0){
                direction = 1;
            }
        }
    }

    private void move(){
        button.setBounds(x,y, Settings.WIDTH_SQUARE, Settings.HEIGHT_SQUARE);
        try{
            Thread.sleep(Settings.DELAY_SQUARE);
        } catch(InterruptedException e){
            e.printStackTrace();
        }
    }

    private void initButton(){
        Random random = new Random();
        Color[] color = {new Color(0,0,0), new Color(255,0,0), new Color(0,255,0),new Color(255,165,122),
                //new Color(255,215,0),new Color(0,0,255),new Color(255,0,255),
                new Color(190,190,190)};
        button = new JButton("" + (++Settings.counter));
        id = Integer.parseInt(button.getText());
        button.setLocation(x,y);
        button.setSize(Settings.WIDTH_SQUARE, Settings.HEIGHT_SQUARE);
        button.setMargin(new Insets(0,0,0,0));
        button.setForeground(Settings.COLOR_TEXT_SQUARE);
        button.setBackground(color[random.nextInt(color.length)]);
        contentPane.add(button);
    }
}