import java.util.*;

public class Algorithm<T extends Detail> {

    private ArrayList<T> result = null;                                                    //оптимальное расписание
    private int totalTime;

    public Algorithm(List<T> details){
        result = optimize(details);
        totalTime = timing(result);
    }


    private ArrayList<T> optimize(List<T> details){

        Queue<T> left = new LinkedList<>();                                                //левая часть
        Deque<T> right = new LinkedList<>();                                               //правая часть

        for(T detail : details){                                                            //для каждой детали из списка
            if(detail.getFirstTime() <= detail.getSecondTime()) {                           //если время на первом станке меньше
                                                                                            //либо равно времени на втором
                left.offer(detail);                                                       //добавить в левую часть
            } else {                                                                        //иначе
                right.addFirst(detail);                                                        //добавить в правую часть
            }
        }

        return sortingParts((List<T>) left, (List<T>) right);
    }


    //сортировка левой и правой части
    private ArrayList<T> sortingParts(List<T> left, List<T> right){

        Collections.sort(left, new Comparator<T>() {                                       //сортировка левой части
            @Override
            public int compare(T o1, T o2) {
                return o1.getFirstTime() - o2.getFirstTime();                              //по времени на первом станке
            }
        });

        Collections.sort(right, new Comparator<T>() {                                      //сортировка правой части
            @Override
            public int compare(T o1, T o2) {
                return o2.getSecondTime() - o1.getSecondTime();                            //по времени на втором станке
            }
        });

        ArrayList<T> details = new ArrayList<>(left);
        details.addAll(right);

        return details;
    }

    //вычисление времени обработки деталей
    private int timing(ArrayList<T> list){
        int time = 0;

        Iterator<T> detail = list.iterator();

        while (detail.hasNext()){
            time += detail.next().getFirstTime();
        }

        time += list.get(list.size()-1).getSecondTime();

        return time;
    }


    public ArrayList<T> getResult(){
        return result;
    }

    public int getTotalTime(){
        return totalTime;
    }

}