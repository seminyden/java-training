public class Detail {

    private int id;
    private int firstTime;
    private int secondTime;

    public Detail(int id, int firstTime, int secondTime){
        this.id = id;
        this.firstTime = firstTime;
        this.secondTime = secondTime;
    }


    public int getFirstTime() {
        return firstTime;
    }

    public int getSecondTime() {
        return secondTime;
    }

    @Override
    public String toString(){
        return id + "(" + firstTime + "," + secondTime + ")";
    }
}