import java.util.*;

public class Test {

    static List<Detail> detailList = new ArrayList<>();

    public static void createDetails(){
        detailList.add(new Detail(1,1,5));
        detailList.add(new Detail(2,6,4));
        detailList.add(new Detail(3,2,3));
        detailList.add(new Detail(4,6,3));
        detailList.add(new Detail(5,9,1));
        detailList.add(new Detail(6,4,5));
        detailList.add(new Detail(7,2,2));
        detailList.add(new Detail(8,7,7));
    }

    public static void main(String[] args){
        createDetails();

        Algorithm<Detail> algorithm = new Algorithm<>(detailList);


        System.out.println("Details: " + detailList);
        System.out.println("Result: " + algorithm.getResult());
        System.out.println("Time: " + algorithm.getTotalTime());
    }
}
