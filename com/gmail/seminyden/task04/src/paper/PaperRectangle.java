package paper;

import figure.Figure;
import figure.Rectangle;
import paper.paint.*;

import figure.exceptions.*;

/**
 * Class paper rectangle(in this case square)
 */
public class PaperRectangle extends Rectangle implements Paper {

    /** for success serializable*/
    private static final long serialVersionUID = 1L;

    /** repainting counter creation*/
    private PaintCounter paintCounter = new PaintCounter();

    /** sets the default shape color*/
    private Color color;


    /**
     * Paper rectangle(in this case square) constructor, side initialization
     *
     * @param side side
     */

    /**
     * Paper rectangle(in this case square) constructor, side initialization
     *
     * @param side side
     * @throws IncorrectSizeException side mustn't be less than 0 and more than 1000
     */
    public PaperRectangle(double side) throws IncorrectSizeException {
        super(side);
    }


    /**
     * Designer of a rectangle(in this case square) of paper cut from another shape
     *
     * @param paperFigure the figure from which the cut
     * @throws IncorrectSizeException side mustn't be less than 0 and more than 1000
     */
    public PaperRectangle(Paper paperFigure) throws IncorrectSizeException {
        super((Figure) paperFigure);
        this.color = paperFigure.getColor();
        this.paintCounter.setPaintCount(paperFigure.getPaintCount());
    }


    /**
     * Returns the number of repaints of a given shape
     *
     * @return number of repaints
     */
    public int getPaintCount(){
        return paintCounter.getPaintCount();
    }


    /**
     * Repaints the rectangle(in this case square) to the specified color
     *
     * @param color color to repaint
     */
    @Override
    public void setColor(String color) {

        if(color == null){
            throw new NullPointerException("Color maustn't be null.");
        }

        if(paintCounter.getPaintCount() < paintCounter.getLimitRepainting()) {
            this.color = Color.valueOf(color);
            paintCounter.incrementPaintCount();
        }
    }


    /**
     * Returns the color value of the rectangle(in this case square)
     *
     * @return rectangle(in this case square) color
     */
    @Override
    public Color getColor() {
        return color;
    }


    /**
     * Create a paper rectangle(in this case square) description line
     *
     * @return String description string
     */
    public String toString(){
        return super.toString() + ", color: " + this.getColor() + ", repainted: " + this.paintCounter.getPaintCount() +
                ", limitRepainting: " + this.paintCounter.getLimitRepainting();
    }


    /**
     * Returns a hashcode of a paper rectangle(in this case square)
     *
     * @return int hashcode
     */
    public int hashCode(){
        return 13 * super.hashCode();
    }
}