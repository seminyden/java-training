package paper;

import figure.Circle;
import figure.Figure;
import paper.paint.*;

import figure.exceptions.*;

public class PaperCircle extends Circle implements Paper {

    /** for success serializable*/
    private static final long serialVersionUID = 1L;

    /** repainting counter creation*/
    private PaintCounter paintCounter = new PaintCounter();

    /** sets the default shape color*/
    private Color color;


    /**
     * Paper circle constructor, radius initialization
     *
     * @param radius circle radius
     * @throws IncorrectSizeException
     */
    public PaperCircle(double radius) throws IncorrectSizeException {
        super(radius);
    }


    /**
     * Circle constructor made from paper cut from another shape
     *
     * @param paperFigure the figure from which they cut
     * @throws IncorrectSizeException radius mustn't be less than 0 and more than 1000
     */
    public PaperCircle(Paper paperFigure) throws IncorrectSizeException {
        super((Figure) paperFigure);
        this.color = paperFigure.getColor();
        this.paintCounter.setPaintCount(paperFigure.getPaintCount());
    }


    /**
     * Returns the number of repaints of a given shape
     *
     * @return number of repaints
     */
    public int getPaintCount(){
        return paintCounter.getPaintCount();
    }


    /**
     * Repainting a circle in a given color
     *
     * @param color color to repaint
     */
    @Override
    public void setColor(String color){

        if(color == null){
            throw new NullPointerException("Color mustn't be null.");
        }

        if(paintCounter.getPaintCount() < paintCounter.getLimitRepainting()) {
            this.color = Color.valueOf(color);
            paintCounter.incrementPaintCount();
        }
    }


    /**
     * Returns the color value of a circle
     *
     * @return color circle color
     */
    @Override
    public Color getColor() {
        return color;
    }


    /**
     * Create a paper circle description line
     *
     * @return String description string
     */
    public String toString(){
        return super.toString() + ", color: " + this.getColor() + ", repainted: " + this.paintCounter.getPaintCount() +
                ", limitRepainting: " + this.paintCounter.getLimitRepainting();
    }


    /**
     * Returns a hashcode of a circle from paper
     *
     * @return int hashcode
     */
    public int hashCode(){
        return 15 * super.hashCode();
    }
}