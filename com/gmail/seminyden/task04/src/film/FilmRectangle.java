package film;

import figure.Figure;
import figure.Rectangle;

import figure.exceptions.*;


/**
 * Class film rectangle(in this case square)
 */
public class FilmRectangle extends Rectangle implements Film {

    /** for success serializable*/
    private static final long serialVersionUID = 1L;

    /**
     * Film rectangle(in this case square) constructor, initialization of sides
     *
     * @param side side
     * @throws IncorrectSizeException side mustn't be less than 0 and more than 1000
     */
    public FilmRectangle(double side) throws IncorrectSizeException {
        super(side);
    }


    /**
     * Constructor of a rectangle(in this case square) from a film cut from another shape
     *
     * @param filmFigure the figure from which the cut
     * @throws IncorrectSizeException side mustn't be less than 0 and more than 1000
     */
    public FilmRectangle(Film filmFigure) throws IncorrectSizeException{
        super((Figure) filmFigure);
    }


    /**
     * Returns the hash of the rectangle(in this case square) from the film
     *
     * @return int hashcode
     */
    public int hashCode(){
        return 7 * super.hashCode();
    }
}