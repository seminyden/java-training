package io.exceptions;

public class SetFileException extends Exception {

    private static final long serialVersionUID = 1L;

    public SetFileException(){}

    public SetFileException(String message){
        super(message);
    }

}
