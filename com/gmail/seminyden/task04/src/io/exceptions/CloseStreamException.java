package io.exceptions;

public class CloseStreamException extends Exception{

    private static final long serialVersionUID = 1L;

    public CloseStreamException(){}

    public CloseStreamException(String message){
        super(message);
    }
}
