package io.exceptions;

import java.io.IOException;

public class GetIOException extends IOException {

    private static final long serialVersionUID = 1L;

    public GetIOException(){}

    public GetIOException(String message){
        super(message);
    }
}