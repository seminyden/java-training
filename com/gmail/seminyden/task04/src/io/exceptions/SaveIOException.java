package io.exceptions;

import java.io.IOException;

public class SaveIOException extends IOException {

    private static final long serialVersionUID = 1L;

    public SaveIOException(){}

    public SaveIOException(String message){
        super(message);
    }
}
