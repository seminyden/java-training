import box.Box;
import figure.*;
import paper.*;
import film.*;
import paper.paint.Color;

import figure.exceptions.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * Test class
 */
public class Test {

    private static Box box = null;

    //Создает список случаных фигур
    private static ArrayList<Figure> createRandomFigures (int count) throws IncorrectSizeException {

       ArrayList<Figure> list = new ArrayList<>();
        for(int i = 1; i <= count; i++){
            int random = (int) (Math.random() * 6) + 1;

            switch(random){
                case 1:
                    PaperCircle pc = new PaperCircle(i);
                    pc.setColor(randomColor().toString());
                    list.add(pc);
                    break;
                case 2:
                    PaperRectangle pr = new PaperRectangle(i);
                    pr.setColor(randomColor().toString());
                    list.add(pr);
                    break;
                case 3:
                    PaperTriangle pt = new PaperTriangle(i);
                    pt.setColor(randomColor().toString());
                    list.add(pt);
                    break;
                case 4:
                    list.add(new FilmCircle(i));
                    break;
                case 5:
                    list.add(new FilmRectangle(i));
                    break;
                case 6:
                    list.add(new FilmTriangle(i));
                    break;
            }
        }

        return list;
    }


    //возвращает случайный цвет
    private static Color randomColor(){
        int random = new Random().nextInt(Color.values().length);
        return Color.values()[random];
    }


    /**
     *
     * в классе PaintCounter =>     private transient int paintCount;
     *                              private static int limitRepainting = 1;
     *
     * @param args no args
     */
    public static void main(String[] args){

        box = Box.getInstanceBox();

        System.out.println("****************** ADDED FIGURES *********************");

        ArrayList<Figure> figures = null;
        Iterator figure = null;

        try {
            figures = createRandomFigures(18);
            figure = figures.iterator();

            while (figure.hasNext()) {
                box.addFigure((Figure) figure.next());
            }

        } catch(Exception ex){

            ex.getStackTrace();
            System.err.println(ex.getMessage());
        }

        figure = null;

        System.out.println(box);

        System.out.println();
        System.out.println("****************** SERIALIZATION 'src\\instance\\boxFigures' ********************");

        try {
            box.saveFiguresToFile("src\\instance\\boxFigures");

            //box.saveFiguresToFile(null);                                        //NullArgumentException

            System.out.println("success");

        } catch(Exception ex){

            ex.getStackTrace();
            System.err.println(ex.getMessage());
        }

        System.out.println();
        System.out.println("************** ADD FIGURE TO BOX *******************");

        try{
            box.addFigure(new PaperRectangle(19));

            //box.addFigure(new PaperTriangle(20));

            //box.addFigure(new FilmCircle(21));                                       //BoxIsFullException

            //box.addFigure(null);                                                   //NullArgumentException

            //box.addFigure(new FilmTriangle(-20));                                  //IncorrectSizeException

            //box.addFigure(figures.get(1));                                          //FigureIsRepeatedException

            System.out.println("success");

        } catch(Exception ex){

            ex.getStackTrace();
            System.err.println(ex.getMessage());
        }

        System.out.println();
        System.out.println("****************** VIEW FIGURE BY NUMBER *********************");

        try{

            System.out.println(box.viewFigureByNumber(10));

            //System.out.println(box.viewFigureByNumber(-10));                    //IncorrectArgumentException

            //System.out.println(box.viewFigureByNumber(30));                     //IncorrectArgumentException


        } catch(Exception ex){

            ex.getStackTrace();
            System.err.println(ex.getMessage());
        }

        System.out.println();
        System.out.println("****************** EXTRACTION FIGURE BY NUMBER ********************");

        Figure extract = null;

        try{

            extract = box.extractionFigure(10);

            //extract = box.extractionFigure(-10);                    //IncorrectArgumentException

            //box.boxClear();                                         //removed all figures from box
            //extract = box.extractionFigure(10);                     //BoxIsEmptyException

            System.out.println(extract);

        } catch(Exception ex){

            ex.getStackTrace();
            System.err.println(ex.getMessage());
        }

        System.out.println();
        System.out.println("************** REPLACE FIGURE *******************");

        Figure replace = null;

        try{

            replace = new FilmTriangle(100);
            //replace = new FilmTriangle(-100);                                     //IncorrectSizeException

            replace = box.replaceFigure(3,replace);


            System.out.println(replace);
            System.out.println(box.viewFigureByNumber(3));

            //box.boxClear();                                                       //removed all figures from box
            //replace = box.replaceFigure(3,replace);                               //BoxIsEmptyException

            //replace = box.replaceFigure(-3,replace);                                //IncorrectArgumentException

            replace = figures.get(5);
            replace = box.replaceFigure(3,replace);                           //FigureIsRepeatedException

            //replace = box.replaceFigure(3,null);                                 //NullArgumentException

        } catch(Exception ex){

            ex.getStackTrace();
            System.err.println(ex.getMessage());
        }

        System.out.println();
        System.out.println("**************** FIND FIGURE BY PATTERN ********************");

        try{

            Figure find = box.toFindFigure(figures.get(11));

            System.out.println(find);

            //box.boxClear();                                                                 //Removed all figures from box
            //System.out.println(box.toFindFigure(figures.get(11)));                        //BoxIsEmptyException

            System.out.println(box.toFindFigure(null));                                     //NullArgumentException

        } catch(Exception ex){

            ex.getStackTrace();
            System.err.println(ex.getMessage());
        }

        System.out.println();
        System.out.println("**************** DESERIALIZATION 'src\\instance\\boxFigures' ********************");

        try{

            box = Box.getInstanceBoxFromFile("src\\instance\\boxFigures");

            //box = Box.getInstanceBoxFromFile(null);                                   //NullArgumentException

            //box = Box.getInstanceBoxFromFile("src\\instance\\boxFigur124235es");      //SetFileException

        } catch(Exception ex){

            ex.getStackTrace();
            System.err.println(ex.getMessage());
        }

        System.out.println(box);

    }
}