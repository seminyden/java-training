package figure;

import figure.exceptions.*;

/**
 * Class circle
 */
public abstract class Circle extends Figure {

    /** for success serializable*/
    private static final long serialVersionUID = 1L;

    /**radius of the circle*/
    private double radius;


    /**
     * Class constructor, initializes the radius of the circle
     *
     * @param radius initialization of circle radius
     * @throws IncorrectSizeException radius mustn't be less than 0 and more than 1000
     */
    public Circle(double radius) throws IncorrectSizeException {
        setRadius(radius);
    }


    /**
     * Class constructor, for cutting a circle from another shape
     *
     * @param figure the figure from which they cut
     * @throws IncorrectSizeException radius mustn't be less than 0 and more than 1000
     */
    public Circle(Figure figure) throws IncorrectSizeException {
        setRadius(calculationSize(figure));
    }


    /**
     * Calculating the radius of a circle to cut it out of another shape
     *
     * @param figure the object from which we cut
     * @return double radius
     */
    public double calculationSize(Figure figure) {

        if(figure == null){
            throw new NullPointerException("Figure mustn't be null.");
        }

        return Math.sqrt((figure.getArea() / 2) / Math.PI);         //площадь фигуры в которую вписываем делим на два
    }                                                               //по формуле находим радиус круга через площадь


    /**
     * Sets the radius value
     *
     * @param radius circle radius
     * @throws IncorrectSizeException radius mustn't be less than 0 and more than 1000
     */
    public void setRadius(double radius) throws IncorrectSizeException {

        if(radius <= 0){
            throw new IncorrectSizeException("Radius mustn't be less than 0: " + radius + " < 0");
        }

        if(radius > 1000){
            throw new IncorrectSizeException("Radius mustn't be more than 1000: " + radius + " > 1000");
        }

        this.radius = radius;
    }


    /**
     * Returns the radius of a circle
     *
     * @return double radius of a circle
     */
    public double getRadius() {
        return radius;
    }


    /**
     * Calculation of the area of a circle
     *
     * @return double value of the area of the circle
     */
    @Override
    public double getArea() {
        double area = Math.PI * radius *radius;
        return area;
    }


    /**
     * Calculation of circle circumference
     *
     * @return double value of circle circumference
     */
    @Override
    public double getPerimetr() {
        double perimetr = 2 * Math.PI * radius;
        return perimetr;
    }


    /**
     * Creating a circle description line
     *
     * @return String line of circle description
     */
    public String toString(){
        return super.toString() + ", radius = " + getRadius();
    }


    /**
     * Checking the equality of two objects
     *
     * @param object object to compare
     * @return result of comparing two figures
     */
    @Override
    public boolean equals(Object object){
        if(!super.equals(object)){
            return false;
        }
        Circle circle = (Circle) object;
        return this.getRadius() == circle.getRadius();
    }

    /**
     * Returns a hashcode of a shape
     *
     * @return int hashcode
     */
    @Override
    public int hashCode() {
        return (int)(17*radius);
    }
}