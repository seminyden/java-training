package box.exceptions;

public class FigureIsRepeatedException extends Exception {

    private static final long serialVersionUID = 1L;

    public FigureIsRepeatedException(){}

    public FigureIsRepeatedException(String message){
        super(message);
    }
}
