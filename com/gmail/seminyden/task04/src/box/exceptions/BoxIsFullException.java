package box.exceptions;

public class BoxIsFullException extends Exception {

    private static final long serialVersionUID = 1L;

    public BoxIsFullException(){}

    public BoxIsFullException(String message){
        super(message);
    }
}
