package box.exceptions;

public class BoxIsEmptyException extends Exception {

    private static final long serialVersionUID = 1L;

    public BoxIsEmptyException(){}

    public BoxIsEmptyException(String message){
        super(message);
    }
}
