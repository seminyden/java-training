package box;

import figure.Circle;
import figure.Figure;
import film.Film;

import io.exceptions.*;
import box.exceptions.*;

import static io.FileHandler.*;

import java.util.ArrayList;

public class Box{

    /**box capacity*/
    private int boxCapacity = 20;

    /**list with figures*/
    public ArrayList<Figure> figuresList;

    /**instance box*/
    private static final Box instanceBox = new Box();


    private Box(){
        this.figuresList = new ArrayList<>();
    }


    /**
     * Returns an instance of the box class
     *
     * @return Box instance
     */
    public static Box getInstanceBox(){
        return instanceBox;
    }


    /**
     * Getting instance with figures from saved file
     *
     * @param pathInstFile path to file with box figures
     *
     * @return instance box with figure
     * @throws SetFileException File not found
     * @throws InstanceClassException Instance class not found
     * @throws CloseStreamException failed clos stream
     * @throws GetIOException io exception
     */
    public static Box getInstanceBoxFromFile(String pathInstFile)
            throws SetFileException, InstanceClassException,
            CloseStreamException, GetIOException{

        if(pathInstFile == null) {
            throw new NullPointerException("Path to file mustn't be null.");
        }

        instanceBox.figuresList.clear();
        ArrayList<Figure> temp = (ArrayList<Figure>) get(pathInstFile);

        for(int i = 0; i < instanceBox.boxCapacity; i++){
            if(i < temp.size()) {
                instanceBox.figuresList.add(temp.get(i));
            }
        }

        return instanceBox;
    }


    /**
     * Write list with figures to file
     *
     * @param pathInstFile path to the file with figures
     * @throws SetFileException File not found
     * @throws CloseStreamException Failed close stream
     * @throws SaveIOException IO exception
     */
    public void saveFiguresToFile(String pathInstFile)
            throws SetFileException, CloseStreamException, SaveIOException {

        if(pathInstFile == null) {
            throw new NullPointerException("Path to file mustn't be null.");
        }

        save(figuresList,pathInstFile);
    }


    /**
     * Adding a shape to the box
     *
     * @param figure the figure to be added to the box
     * @throws BoxIsFullException box is full
     * @throws FigureIsRepeatedException this figure is repeated
     */
    public void addFigure(Figure figure) throws BoxIsFullException, FigureIsRepeatedException {

        if(figure == null){
            throw new NullPointerException("Figure mustn't be null.");
        }

        if(figuresList.size() >= boxCapacity){
            throw new BoxIsFullException("Box is full.");
        }

        for (Figure inBox : figuresList) {
            if (inBox.equals(figure)) {
                throw new FigureIsRepeatedException("This figure is already in the box.");
            }
        }

        figuresList.add(figure);
    }


    /**
     * View the figures in the box by number
     *
     * @param number figure number in the box
     * @return String with the result
     * @throws BoxIsEmptyException box is empty
     */
    public Figure viewFigureByNumber(int number) throws BoxIsEmptyException {

        if(figuresList.isEmpty()) {
            throw new BoxIsEmptyException("Box is empty");
        }

        if(number <= 0 || figuresList.size() < number) {
            throw new ArrayIndexOutOfBoundsException("number mustn't be less than 0 or more than figureList.size ");
        }

        return figuresList.get(--number);
    }


    /**
     * Extraction of a figure from a box by number
     *
     * @param number figure number in the box
     * @return extracted figure
     * @throws BoxIsEmptyException box is empty
     */
    public Figure extractionFigure(int number) throws BoxIsEmptyException{

        if(figuresList.isEmpty()) {
            throw new BoxIsEmptyException("Box is empty.");
        }

        if(number <= 0 || figuresList.size() < number) {
            throw new ArrayIndexOutOfBoundsException("Number mustn't be less than 0 and more than figureList.size" );
        }

        Figure temp = figuresList.get(--number);
        figuresList.remove(number);
        return temp;
    }

    /**
     * Figure replacement by number
     *
     * @param number figure number in the box
     * @param figure the figure to which we replace
     *
     * @return the figure that was at that place
     * @throws BoxIsEmptyException box is empty
     * @throws FigureIsRepeatedException this figure is already in the box
     */
    public Figure replaceFigure(int number,
                                Figure figure)
            throws BoxIsEmptyException, FigureIsRepeatedException {

        if(figuresList.isEmpty()) {
            throw new BoxIsEmptyException("Box is empty.");
        }

        if(figure == null) {
            throw new NullPointerException("Figure mustn't be null.");
        }

        if(number < 0 || figuresList.size() < number) {
            throw new ArrayIndexOutOfBoundsException("Position number mustn't be less than 0 and more than figureList.size()" );
        }

        for (Figure inBox : figuresList) {
            if (inBox.equals(figure)) {
                throw new FigureIsRepeatedException("This figure is already in the box.");
            }
        }

        Figure temp = figuresList.get(--number);
        figuresList.set(number,figure);
        return temp;
    }


    /**
     * Search by pattern
     *
     * @param figure pattern for search
     * @return Figure find result
     * @throws BoxIsEmptyException box is empty
     */
    public Figure toFindFigure(Figure figure) throws BoxIsEmptyException {

        if(figuresList.isEmpty()) {
            throw new BoxIsEmptyException("Box is empty.");
        }

        if(figure == null) {
            throw new NullPointerException("Figure mustn't be null.");
        }

        for(Figure inBox: figuresList){
            if(figure.equals(inBox)){
                return inBox;
            }
        }

        return null;
    }


    /**
     * Shows the number of shapes in a box
     *
     * @return String string with the result
     */
    public int countFigures(){
        return figuresList.size();
    }


    /**
     * Calculates the total area of figures lying in a box
     *
     * @return String string with total area
     */
    public double totalArea(){
        double totalArea = 0;
        for (Figure inBox : figuresList){
            totalArea += inBox.getArea();
        }
        return totalArea;
    }


    /**
     * Calculates the total perimeter of figures lying in a box
     *
     * @return String string with total perimeter
     */
    public double totalPerimetr(){
        double totalPerimetr = 0;
        for (Figure inBox : figuresList){
            totalPerimetr += inBox.getPerimetr();
        }
        return totalPerimetr;
    }


    /**
     * Extraction all circles from the box
     *
     * @return ArrayList with all circles from the box
     */
    public ArrayList extractionAllCircles(){
        ArrayList<Circle> circleList = new ArrayList<>();

        for(Figure inBox : figuresList){
            if(inBox instanceof Circle){
                circleList.add((Circle) inBox);
            }
        }
        return circleList;
    }


    /**
     * Extraction all film shapes from the box
     *
     * @return ArrayList with all film shapes from the box
     */
    public ArrayList extractionAllFilmFigures(){
        ArrayList<Film> filmList = new ArrayList<>();

        for(Figure inBox : figuresList){
            if(inBox instanceof Film){
                filmList.add((Film) inBox);
            }
        }
        return filmList;
    }

    /**
     * Removing all figures in the box
     */
    public void boxClear(){
        figuresList.clear();
    }


    /**
     * Create description line
     * @return String description line
     */
    public String toString(){
        StringBuilder box = new StringBuilder();
        for (Figure inBox : figuresList){
            box.append((figuresList.indexOf(inBox) + 1));
            box.append(" ");
            box.append(inBox.toString());
            box.append("\n");
        }
        return new String(box);
    }
}